import {HTTP_BEARER} from './../util/http-commons';
import router from './../router'
import adminAuth from './../util/authHandler';
import OrdersHandler from './../util/OrdersHandler';
import DeliveryHandler from './../util/DeliveryHandler';
import Toast from './../util/Toast';
import MapHelper from './../util/MapHelper'
import ProductsHandler from "../util/ProductsHandler";
import {getCustomers, getChats} from "../util/ChatHandler";
import UsersHandler from "../util/UsersHandler";

export const actions = {


  login(context, payload) {
    adminAuth.login(payload, function (error, data) {
      if (error) {
        Toast.ERROR("Invalid Credentials", 4000, 'rounded');
        return;
      }
      context.commit('login', data);
      router.push({path : '/dashboard', query : {}});
    });
    //context.commit('login', payload);
  },



  logout(context, payload) {
    context.commit('logout', payload);
  },


  updateFCMId(context, payload){
    UsersHandler.updateUserFCMId(payload, function (error, response) {

    });
    context.commit('UPDATE_USER_FCMID', payload);
  },


  getMeasurements(context, payload) {
    ProductsHandler.getMeasurements({}, function (error, response) {
      if (error) {
        if (error.response && error.response.data){
          Toast.ERROR(error.response.data.message, 7000);
        }
        console.log(error.response);
        return;
      }
      //let shipmentMap = MapHelper.ConvertArrayToMap(response, 'id');
      context.commit('getMeasurements', response);
    });
  },




  getShipments(context, payload) {
    OrdersHandler.getShipments({}, function (error, response) {
      if (error) {
        if (error.response && error.response.data){
          Toast.ERROR(error.response.data.message, 7000);
        }
        console.log(error.response);
        return;
      }
      let shipmentMap = MapHelper.ConvertArrayToMap(response, 'id');
      context.commit('getShipments', shipmentMap);
      //Toast.SUCCESS('Success: Shipments Fetched', 4000, 'rounded');
    });
  },




  createShipment(context, payload) {
    console.log('createShipment');
    context.commit('createShipment', payload);
  },




  updateshipment(context, payload) {
    OrdersHandler.updateShipmentStatus(payload, function (error, response) {
      if (error) {
        if (error.response && error.response.data){
          Toast.ERROR(error.response.data.message, 7000);
        }
        console.log(error.response);
        return;
      }
      Toast.SUCCESS('Shipment was updated', 4000, 'rounded');
      context.commit('updateshipment', response)
    });
  },




  getDeliveryAgents(context, payload) {
    DeliveryHandler.getDeliveryAgents(payload, function (error, response) {
      if (error) {
        if (error.response && error.response.data){
          Toast.ERROR(error.response.data.message, 7000);
        }
        console.log(error.response);
        return;
      }
      //Toast.SUCCESS('Success: Delivery Agents Fetched', 4000, 'rounded');
      let deliveryAgentsMap = MapHelper.ConvertArrayToMap(response, 'id');
      context.commit('getDeliveryAgents', deliveryAgentsMap)
    });
  },




  assignShipmentForDelivery(context, payload) {
    OrdersHandler.assignShipmentForDelivery(payload, function (error, response) {
      if (error) {
        if (error.response && error.response.data){
          Toast.ERROR(error.response.data.message, 7000);
        }
        console.log(error.response);
        return;
      }
      Toast.SUCCESS('Success: Shipment assigned for Delivery', 4000, 'rounded');
      context.commit('updateshipment', response)
    });
  },




  saveShipmentToCache(context, payload){
    context.commit('saveShipmentToCache', payload);
  },



  popShipmentFromCache(context, payload){
    context.commit('popShipmentFromCache', payload);
  },


  async getCustomers(context) {
    let customers = await getCustomers();
    let customersMap = MapHelper.ConvertArrayToMap(customers, 'id');
    context.commit('SET_ONLINE_CUSTOMERS', customersMap);
    return Promise.resolve();
  },

  async getChats(context, payload){
    try {
      let chats = await getChats(payload);
      context.commit('SET_CHATS', {clientId: payload.id, messages: chats});
    } catch (e) {
      context.commit('SET_CHATS', {clientId: payload.id, messages: []});
    }
  },
}
