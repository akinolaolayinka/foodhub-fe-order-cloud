export const state = {
  app : {
    toolbar : {
      title : 'FoodHub Orders'
    },
    navigation : {
      users : false,
      clients : false,
    }
  },
  fcmId: null,
  user : null,
  token : null,
  count : 0,
  shipments : {},
  delivery_agents : {},
  measurements:[],
  unsent_shipments:{}, //if send shipment fails, we store here
  users:[], //this is used when we want to bind a shipment to a user,
  customers:{}, //this stores the online customers so we can chat
  chats:{}, //this stores the online customers so we can chat
  typing:{} //holds the state of whoever is typing
};
