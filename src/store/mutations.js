import Vue from "vue";
import moment from "moment";

export const mutations = {
  logout(state) {
    state.user = null;
    state.token = null;
    state.shipments = {};
    state.delivery_agents = {};
  },
  login(state, payload) {
    Vue.set(state, 'user', payload.user);
    state.token = payload.token;
  },
  getMeasurements(state, payload) {
    Vue.set(state, 'measurements', payload);
  },
  getShipments(state, payload) {
    Vue.set(state, 'shipments', payload);
  },
  updateshipment(state, payload){
    //state.shipments[payload.id]=payload;
    Vue.set(state.shipments, payload.id, payload);
  },
  createShipment(state, payload){
    //state.shipments[state.s.id]=state.s;
    //state.shipments[payload.id]=payload;
    Vue.set(state.shipments, payload.id, payload);
  },
  getDeliveryAgents(state, payload){
    Vue.set(state, 'delivery_agents', payload);
  },
  saveShipmentToCache(state, payload){
    let datetime = moment().format("YYYYMMDDHHmmss");
    Vue.set(state.unsent_shipments, datetime, payload);
  },
  popShipmentFromCache(state, payload){
    //delete state.unsent_shipments[payload];
    Vue.delete(state.unsent_shipments, payload);
  },

  SET_ONLINE_CUSTOMERS(state, payload){
    Vue.set(state, 'customers', payload);
  },

  SET_CUSTOMER_ONLINE_STATUS(state, payload){
    if (state.customers[payload.id]) {
      Vue.set(state.customers[payload.id], 'online', payload.online);
    }
  },

  SET_CHATS(state, payload){
    Vue.set(state.chats, payload.clientId, payload.messages);
  },

  INSERT_CHAT(state, payload){
    //add message to recipients thread
    let messages = state.chats[payload.recipient.id] || [];
    messages = [...messages, payload];
    Vue.set(state.chats, payload.recipient.id, messages);

    //add message to senders thread
    messages = state.chats[payload.sender.id] || [];
    messages = [...messages, payload];
    Vue.set(state.chats, payload.sender.id, messages);

    if (state.customers[payload.sender.id]){
      Vue.set(state.customers[payload.sender.id], 'lastMessage', payload);
      Vue.set(state.customers[payload.recipient.id], 'lastMessage', payload);
    }
  },

  UPDATE_ISTYPING(state, payload) {
    Vue.set(state.typing, payload.sender, payload.isTyping);
  },

  UPDATE_USER_FCMID(state, payload) {
    Vue.set(state, 'fcmId', payload.fcmId);
    Vue.set(state.user, 'fcmId', payload.fcmId);
  },
};
