import Vue from 'vue'
import Vuex from 'vuex'
import {actions} from "./actions";
import {state} from "./state";
import {mutations} from "./mutations";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const getters = {
  getUser(state) {
    return state.user
  },
  getToken(state) {
    return state.token
  }
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  plugins : [
    createPersistedState({
      key: 'order-cloud'
    })]
})
