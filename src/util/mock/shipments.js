export const shipment = () => {
  return {
    "144": {
      "progression": [],
      "order": {
        "id": 119,
        "orderNo": "FH-0216021626",
        "order_creation_time": "2019-02-16 02:16:26",
        "order_delivery_time": "2019-02-16T07:16:26.000Z",
        "status": "delivered",
        "shipment": 144,
        "user": {
          "id": 2,
          "firstName": "Akinola",
          "lastName": "Olayinko",
          "email": "liljoeengr22@yahoo.com",
          "phoneNo": "08024708087",
          "deviceId": null,
          "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
          "fcmId": "",
          "facebookId": "10214898335554789",
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-18 06:41:57",
          "last_visit_point_date": "2019-02-18 02:01:07",
          "foodCoins": 103,
          "cancelled_orders": 2,
          "referrer_code": "akinola_447",
          "referred_by": "",
          "online": true,
          "createdAt": "2018-01-06T20:55:06.000Z",
          "updatedAt": "2019-02-18T11:42:53.000Z",
          "address": {
            "id": 3,
            "name": "12 Road 26, Ibadan, Oyo, Nigeria",
            "description": "Ikota shopping complex",
            "street": "",
            "google_map_street": "Road 26",
            "latitude": "7.434061666666666",
            "longitude": "3.953156666666666",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 9,
              "name": "Alao Akala Way",
              "latitude": "7.443527",
              "longitude": "3.951607",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 264,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 1,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 600,
            "status": null
          },
          {
            "id": 265,
            "productvariant": {
              "id": 2,
              "name": "Honeywell Semolina",
              "short_description": "Tasty Semolina. Honeywell Brand",
              "description": "Tasty Semolina. Honeywell Brand",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/honeywell_semolina_2kg.png",
              "stockcount": 10,
              "status": true,
              "isAvailable": true,
              "askance_index": 23,
              "tags": "honeywell semolina semo semovita",
              "category": 1,
              "product": 4
            },
            "quantity": 1,
            "measurement": {
              "id": 7,
              "cost_price": 345,
              "per_unit_cost": 360,
              "old_per_unit_cost": 450,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/honeywell_semolina_1kg.png",
              "status": true,
              "productvariant": 2,
              "scale": {
                "id": 24,
                "name": "1Kg Bag",
                "description": null,
                "image": null,
                "status": null
              }
            },
            "price": 360,
            "status": null
          },
          {
            "id": 266,
            "productvariant": {
              "id": 2,
              "name": "Honeywell Semolina",
              "short_description": "Tasty Semolina. Honeywell Brand",
              "description": "Tasty Semolina. Honeywell Brand",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/honeywell_semolina_2kg.png",
              "stockcount": 10,
              "status": true,
              "isAvailable": true,
              "askance_index": 23,
              "tags": "honeywell semolina semo semovita",
              "category": 1,
              "product": 4
            },
            "quantity": 1,
            "measurement": {
              "id": 8,
              "cost_price": 660,
              "per_unit_cost": 670,
              "old_per_unit_cost": 900,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/honeywell_semolina_2kg.png",
              "status": true,
              "productvariant": 2,
              "scale": {
                "id": 25,
                "name": "2Kg Bag",
                "description": null,
                "image": null,
                "status": null
              }
            },
            "price": 670,
            "status": null
          }
        ]
      },
      "address": {
        "id": 145,
        "name": "12 Road 26, Ibadan, Oyo, Nigeria",
        "description": "Ikota shopping complex",
        "street": null,
        "google_map_street": "Ruan Jian Yuan Er Hao Lu",
        "latitude": "7.4397362",
        "longitude": "3.9494594",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 3,
          "name": "Akobo Baptist",
          "latitude": "7.432162",
          "longitude": "3.941496",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 2,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "deviceId": null,
        "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
        "fcmId": "",
        "facebookId": "10214898335554789",
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-18 06:41:57",
        "last_visit_point_date": "2019-02-18 02:01:07",
        "foodCoins": 103,
        "cancelled_orders": 2,
        "referrer_code": "akinola_447",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-06T20:55:06.000Z",
        "updatedAt": "2019-02-18T11:42:53.000Z",
        "address": 3
      },
      "guest": null,
      "deliverTo": {
        "id": 183,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 160,
        "orderCharge": "1630",
        "deliveryCharge": "270",
        "amount": "1900",
        "merchantRef": null,
        "transactionId": null,
        "status": "received",
        "createdAt": "2019-02-16T07:16:26.000Z",
        "updatedAt": "2019-02-23T12:51:44.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "deliveryAgent": {
        "id": 1,
        "firstName": "Adekunle",
        "lastName": "Akinola",
        "email": "olayinka@gmail.com",
        "phoneNo": "08024708088",
        "deviceId": "10049c4d-912d-4a5c-a385-35b70aab242e",
        "profilePix": "https://s3.amazonaws.com/foodhub-dev/general/default_avatar.jpg",
        "fcmId": "djh3woMeJK8:APA91bErBOyZ0Os8xYAvLFQyMNli-N1gGtONSD7lYV0dr1xc0nS8HPSGrqF_hPE1WvEEjB8IMuDCyIEj4HSEFLLvZSPXotrNAWYXP_3JHwEfxLPMtZMCXWl3KDma-4RSdvEQwMMbGwwD",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-14 02:48:57",
        "last_visit_point_date": "2019-02-14 02:48:57",
        "foodCoins": 42,
        "cancelled_orders": 4,
        "referrer_code": "olayinka_648",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-03T16:07:01.000Z",
        "updatedAt": "2019-02-26T08:30:47.000Z",
        "address": 2
      },
      "shop": {
        "id": 1,
        "name": "FoodHub Akobo",
        "latitude": "7.432162",
        "longitude": "3.941496",
        "status": true
      },
      "id": 144,
      "deliverycharge": "0",
      "latitude": "7.432162",
      "longitude": "3.941496",
      "eta": 27,
      "status": "delivered",
      "sales_type": "app-sale",
      "shipment_creation_time": "2019-02-16 02:16:26",
      "shipment_delivery_time": "2019-02-16 02:16:26",
      "createdAt": "2019-02-16 02:16:26",
      "updatedAt": "2019-02-23 07:51:48"
    },
    "145": {
      "progression": [],
      "order": {
        "id": 120,
        "orderNo": "FH-0216025425",
        "order_creation_time": "2019-02-16 02:54:25",
        "order_delivery_time": "2019-02-16T07:54:25.000Z",
        "status": "in-transit",
        "shipment": 145,
        "user": {
          "id": 2,
          "firstName": "Akinola",
          "lastName": "Olayinko",
          "email": "liljoeengr22@yahoo.com",
          "phoneNo": "08024708087",
          "deviceId": null,
          "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
          "fcmId": "",
          "facebookId": "10214898335554789",
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-18 06:41:57",
          "last_visit_point_date": "2019-02-18 02:01:07",
          "foodCoins": 103,
          "cancelled_orders": 2,
          "referrer_code": "akinola_447",
          "referred_by": "",
          "online": true,
          "createdAt": "2018-01-06T20:55:06.000Z",
          "updatedAt": "2019-02-18T11:42:53.000Z",
          "address": {
            "id": 3,
            "name": "12 Road 26, Ibadan, Oyo, Nigeria",
            "description": "Ikota shopping complex",
            "street": "",
            "google_map_street": "Road 26",
            "latitude": "7.434061666666666",
            "longitude": "3.953156666666666",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 9,
              "name": "Alao Akala Way",
              "latitude": "7.443527",
              "longitude": "3.951607",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 267,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 2,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 1200,
            "status": null
          },
          {
            "id": 268,
            "productvariant": {
              "id": 13,
              "name": "Yam Flour (Elubo) White",
              "short_description": "Yam Flour",
              "description": "Yam Flour",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/elubo_1.jpg",
              "stockcount": 0,
              "status": true,
              "isAvailable": false,
              "askance_index": 2,
              "tags": "yam flour elubo amala lafun",
              "category": 1,
              "product": 7
            },
            "quantity": 3,
            "measurement": {
              "id": 35,
              "cost_price": null,
              "per_unit_cost": 250,
              "old_per_unit_cost": null,
              "image": "/grains/elubo_1.jpg",
              "status": true,
              "productvariant": 13,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 750,
            "status": null
          },
          {
            "id": 269,
            "productvariant": {
              "id": 14,
              "name": "Fresh Beef (Cow Meat)",
              "short_description": "Fresh Beef (Cow Meat)",
              "description": "Fresh Beef (Cow Meat)",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/dairy/cow-tail-3.jpg",
              "stockcount": 0,
              "status": true,
              "isAvailable": true,
              "askance_index": 8,
              "tags": "cow meat eran malu beef ora isan",
              "category": 6,
              "product": 12
            },
            "quantity": 1,
            "measurement": {
              "id": 38,
              "cost_price": null,
              "per_unit_cost": 1600,
              "old_per_unit_cost": 1700,
              "image": "https://s3.amazonaws.com/foodhub-dev/dairy/cow_meat_2.jpg",
              "status": true,
              "productvariant": 14,
              "scale": {
                "id": 62,
                "name": "Kilo",
                "description": "1Kg = 1 Kilo",
                "image": null,
                "status": true
              }
            },
            "price": 1600,
            "status": null
          }
        ]
      },
      "address": {
        "id": 146,
        "name": "12 Road 26, Ibadan, Oyo, Nigeria",
        "description": "Ikota shopping complex",
        "street": null,
        "google_map_street": "Ruan Jian Yuan Er Hao Lu",
        "latitude": "7.4397362",
        "longitude": "3.9494594",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 3,
          "name": "Akobo Baptist",
          "latitude": "7.432162",
          "longitude": "3.941496",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 2,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "deviceId": null,
        "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
        "fcmId": "",
        "facebookId": "10214898335554789",
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-18 06:41:57",
        "last_visit_point_date": "2019-02-18 02:01:07",
        "foodCoins": 103,
        "cancelled_orders": 2,
        "referrer_code": "akinola_447",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-06T20:55:06.000Z",
        "updatedAt": "2019-02-18T11:42:53.000Z",
        "address": 3
      },
      "guest": null,
      "deliverTo": {
        "id": 184,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 161,
        "orderCharge": "3550",
        "deliveryCharge": "270",
        "amount": "3820",
        "merchantRef": null,
        "transactionId": null,
        "status": "not_yet_paid",
        "createdAt": "2019-02-16T07:54:25.000Z",
        "updatedAt": "2019-02-16T07:54:25.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "deliveryAgent": {
        "id": 2,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "deviceId": null,
        "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
        "fcmId": "",
        "facebookId": "10214898335554789",
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-18 06:41:57",
        "last_visit_point_date": "2019-02-18 02:01:07",
        "foodCoins": 103,
        "cancelled_orders": 2,
        "referrer_code": "akinola_447",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-06T20:55:06.000Z",
        "updatedAt": "2019-02-18T11:42:53.000Z",
        "address": 3
      },
      "shop": {
        "id": 1,
        "name": "FoodHub Akobo",
        "latitude": "7.432162",
        "longitude": "3.941496",
        "status": true
      },
      "id": 145,
      "deliverycharge": "0",
      "latitude": "7.432162",
      "longitude": "3.941496",
      "eta": 0,
      "status": "in-transit",
      "sales_type": "app-sale",
      "shipment_creation_time": "2019-02-16 02:54:25",
      "shipment_delivery_time": "2019-02-16 02:54:25",
      "createdAt": "2019-02-16 02:54:25",
      "updatedAt": "2019-02-23 07:48:08"
    },
    "146": {
      "progression": [],
      "order": {
        "id": 121,
        "orderNo": "FH-0216030459",
        "order_creation_time": "2019-02-16 03:04:59",
        "order_delivery_time": "2019-02-16T08:04:59.000Z",
        "status": "in-transit",
        "shipment": 146,
        "user": {
          "id": 2,
          "firstName": "Akinola",
          "lastName": "Olayinko",
          "email": "liljoeengr22@yahoo.com",
          "phoneNo": "08024708087",
          "deviceId": null,
          "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
          "fcmId": "",
          "facebookId": "10214898335554789",
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-18 06:41:57",
          "last_visit_point_date": "2019-02-18 02:01:07",
          "foodCoins": 103,
          "cancelled_orders": 2,
          "referrer_code": "akinola_447",
          "referred_by": "",
          "online": true,
          "createdAt": "2018-01-06T20:55:06.000Z",
          "updatedAt": "2019-02-18T11:42:53.000Z",
          "address": {
            "id": 3,
            "name": "12 Road 26, Ibadan, Oyo, Nigeria",
            "description": "Ikota shopping complex",
            "street": "",
            "google_map_street": "Road 26",
            "latitude": "7.434061666666666",
            "longitude": "3.953156666666666",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 9,
              "name": "Alao Akala Way",
              "latitude": "7.443527",
              "longitude": "3.951607",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 270,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 2,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 1200,
            "status": null
          },
          {
            "id": 271,
            "productvariant": {
              "id": 13,
              "name": "Yam Flour (Elubo) White",
              "short_description": "Yam Flour",
              "description": "Yam Flour",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/elubo_1.jpg",
              "stockcount": 0,
              "status": true,
              "isAvailable": false,
              "askance_index": 2,
              "tags": "yam flour elubo amala lafun",
              "category": 1,
              "product": 7
            },
            "quantity": 3,
            "measurement": {
              "id": 35,
              "cost_price": null,
              "per_unit_cost": 250,
              "old_per_unit_cost": null,
              "image": "/grains/elubo_1.jpg",
              "status": true,
              "productvariant": 13,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 750,
            "status": null
          },
          {
            "id": 272,
            "productvariant": {
              "id": 14,
              "name": "Fresh Beef (Cow Meat)",
              "short_description": "Fresh Beef (Cow Meat)",
              "description": "Fresh Beef (Cow Meat)",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/dairy/cow-tail-3.jpg",
              "stockcount": 0,
              "status": true,
              "isAvailable": true,
              "askance_index": 8,
              "tags": "cow meat eran malu beef ora isan",
              "category": 6,
              "product": 12
            },
            "quantity": 1,
            "measurement": {
              "id": 38,
              "cost_price": null,
              "per_unit_cost": 1600,
              "old_per_unit_cost": 1700,
              "image": "https://s3.amazonaws.com/foodhub-dev/dairy/cow_meat_2.jpg",
              "status": true,
              "productvariant": 14,
              "scale": {
                "id": 62,
                "name": "Kilo",
                "description": "1Kg = 1 Kilo",
                "image": null,
                "status": true
              }
            },
            "price": 1600,
            "status": null
          }
        ]
      },
      "address": {
        "id": 147,
        "name": "12 Road 26, Ibadan, Oyo, Nigeria",
        "description": "Ikota shopping complex",
        "street": null,
        "google_map_street": "Ruan Jian Yuan Er Hao Lu",
        "latitude": "7.4397362",
        "longitude": "3.9494594",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 3,
          "name": "Akobo Baptist",
          "latitude": "7.432162",
          "longitude": "3.941496",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 2,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "deviceId": null,
        "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
        "fcmId": "",
        "facebookId": "10214898335554789",
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-18 06:41:57",
        "last_visit_point_date": "2019-02-18 02:01:07",
        "foodCoins": 103,
        "cancelled_orders": 2,
        "referrer_code": "akinola_447",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-06T20:55:06.000Z",
        "updatedAt": "2019-02-18T11:42:53.000Z",
        "address": 3
      },
      "guest": null,
      "deliverTo": {
        "id": 185,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 162,
        "orderCharge": "3550",
        "deliveryCharge": "270",
        "amount": "3820",
        "merchantRef": null,
        "transactionId": null,
        "status": "not_yet_paid",
        "createdAt": "2019-02-16T08:04:59.000Z",
        "updatedAt": "2019-02-16T08:04:59.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "shop": {
        "id": 1,
        "name": "FoodHub Akobo",
        "latitude": "7.432162",
        "longitude": "3.941496",
        "status": true
      },
      "id": 146,
      "deliverycharge": "0",
      "latitude": "7.432162",
      "longitude": "3.941496",
      "eta": 0,
      "status": "in-transit",
      "sales_type": "app-sale",
      "shipment_creation_time": "2019-02-16 03:04:59",
      "shipment_delivery_time": "2019-02-16 03:04:59",
      "createdAt": "2019-02-16 03:04:59",
      "updatedAt": "2019-02-23 07:43:17"
    },
    "147": {
      "progression": [],
      "order": {
        "id": 122,
        "orderNo": "FH-0216063039",
        "order_creation_time": "2019-02-16 06:30:39",
        "order_delivery_time": "2019-02-16T11:30:39.000Z",
        "status": "in-transit",
        "shipment": 147,
        "user": {
          "id": 2,
          "firstName": "Akinola",
          "lastName": "Olayinko",
          "email": "liljoeengr22@yahoo.com",
          "phoneNo": "08024708087",
          "deviceId": null,
          "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
          "fcmId": "",
          "facebookId": "10214898335554789",
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-18 06:41:57",
          "last_visit_point_date": "2019-02-18 02:01:07",
          "foodCoins": 103,
          "cancelled_orders": 2,
          "referrer_code": "akinola_447",
          "referred_by": "",
          "online": true,
          "createdAt": "2018-01-06T20:55:06.000Z",
          "updatedAt": "2019-02-18T11:42:53.000Z",
          "address": {
            "id": 3,
            "name": "12 Road 26, Ibadan, Oyo, Nigeria",
            "description": "Ikota shopping complex",
            "street": "",
            "google_map_street": "Road 26",
            "latitude": "7.434061666666666",
            "longitude": "3.953156666666666",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 9,
              "name": "Alao Akala Way",
              "latitude": "7.443527",
              "longitude": "3.951607",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 273,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 7,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 4200,
            "status": null
          }
        ]
      },
      "address": {
        "id": 148,
        "name": "12 Road 26, Ibadan, Oyo, Nigeria",
        "description": "Ikota shopping complex",
        "street": null,
        "google_map_street": "Ruan Jian Yuan Er Hao Lu",
        "latitude": "7.4397362",
        "longitude": "3.9494594",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 15,
          "name": "Elewuro",
          "latitude": "7.467048",
          "longitude": "3.959593",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 2,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "deviceId": null,
        "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
        "fcmId": "",
        "facebookId": "10214898335554789",
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-18 06:41:57",
        "last_visit_point_date": "2019-02-18 02:01:07",
        "foodCoins": 103,
        "cancelled_orders": 2,
        "referrer_code": "akinola_447",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-06T20:55:06.000Z",
        "updatedAt": "2019-02-18T11:42:53.000Z",
        "address": 3
      },
      "guest": null,
      "deliverTo": {
        "id": 186,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 163,
        "orderCharge": "4200",
        "deliveryCharge": "400",
        "amount": "4600",
        "merchantRef": null,
        "transactionId": null,
        "status": "not_yet_paid",
        "createdAt": "2019-02-16T11:30:39.000Z",
        "updatedAt": "2019-02-16T11:30:39.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "shop": {
        "id": 1,
        "name": "FoodHub Akobo",
        "latitude": "7.432162",
        "longitude": "3.941496",
        "status": true
      },
      "id": 147,
      "deliverycharge": "0",
      "latitude": "7.432162",
      "longitude": "3.941496",
      "eta": 0,
      "status": "in-transit",
      "sales_type": "app-sale",
      "shipment_creation_time": "2019-02-16 06:30:39",
      "shipment_delivery_time": "2019-02-16 06:30:39",
      "createdAt": "2019-02-16 06:30:39",
      "updatedAt": "2019-02-23 07:10:45"
    },
    "148": {
      "progression": [],
      "order": {
        "id": 123,
        "orderNo": "FH-0220054607",
        "order_creation_time": "2019-02-20 05:46:07",
        "order_delivery_time": "2019-02-20T10:46:07.000Z",
        "status": "delivered",
        "shipment": 148,
        "user": {
          "id": 30,
          "firstName": "Pasta",
          "lastName": "Lemon",
          "email": "",
          "phoneNo": "08023212123",
          "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
          "profilePix": "",
          "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-20 14:39:45",
          "last_visit_point_date": "2019-02-20 05:30:45",
          "foodCoins": 68,
          "cancelled_orders": 0,
          "referrer_code": "pasta_817",
          "referred_by": "jeremy337",
          "online": true,
          "createdAt": "2018-10-01T15:15:03.000Z",
          "updatedAt": "2019-02-20T19:39:45.000Z",
          "address": {
            "id": 15,
            "name": "Pls let's know",
            "description": "Pls let's know",
            "street": null,
            "google_map_street": "235 Ikorodu Rd, Ilupeju, Lagos, Nigeria",
            "latitude": "6.5538792",
            "longitude": "3.3662943",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 3,
              "name": "Akobo Baptist",
              "latitude": "7.432162",
              "longitude": "3.941496",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 274,
            "productvariant": {
              "id": 2,
              "name": "Honeywell Semolina",
              "short_description": "Tasty Semolina. Honeywell Brand",
              "description": "Tasty Semolina. Honeywell Brand",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/honeywell_semolina_2kg.png",
              "stockcount": 10,
              "status": true,
              "isAvailable": true,
              "askance_index": 23,
              "tags": "honeywell semolina semo semovita",
              "category": 1,
              "product": 4
            },
            "quantity": 1,
            "measurement": {
              "id": 7,
              "cost_price": 345,
              "per_unit_cost": 360,
              "old_per_unit_cost": 450,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/honeywell_semolina_1kg.png",
              "status": true,
              "productvariant": 2,
              "scale": {
                "id": 24,
                "name": "1Kg Bag",
                "description": null,
                "image": null,
                "status": null
              }
            },
            "price": 360,
            "status": null
          }
        ]
      },
      "address": {
        "id": 149,
        "name": "Pls let's know",
        "description": "Pls let's know",
        "street": null,
        "google_map_street": "235 Ikorodu Rd, Ilupeju, Lagos, Nigeria",
        "latitude": "6.5538792",
        "longitude": "3.3662943",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 3,
          "name": "Akobo Baptist",
          "latitude": "7.432162",
          "longitude": "3.941496",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 30,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
        "profilePix": "",
        "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-20 14:39:45",
        "last_visit_point_date": "2019-02-20 05:30:45",
        "foodCoins": 68,
        "cancelled_orders": 0,
        "referrer_code": "pasta_817",
        "referred_by": "jeremy337",
        "online": true,
        "createdAt": "2018-10-01T15:15:03.000Z",
        "updatedAt": "2019-02-20T19:39:45.000Z",
        "address": 15
      },
      "guest": null,
      "deliverTo": {
        "id": 187,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 164,
        "orderCharge": "360",
        "deliveryCharge": "260",
        "amount": "620",
        "merchantRef": null,
        "transactionId": null,
        "status": "received",
        "createdAt": "2019-02-20T10:46:07.000Z",
        "updatedAt": "2019-02-23T12:42:29.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "deliveryAgent": {
        "id": 2,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "deviceId": null,
        "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
        "fcmId": "",
        "facebookId": "10214898335554789",
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-18 06:41:57",
        "last_visit_point_date": "2019-02-18 02:01:07",
        "foodCoins": 103,
        "cancelled_orders": 2,
        "referrer_code": "akinola_447",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-06T20:55:06.000Z",
        "updatedAt": "2019-02-18T11:42:53.000Z",
        "address": 3
      },
      "id": 148,
      "deliverycharge": "260",
      "latitude": "7.382728",
      "longitude": "3.382728",
      "eta": 0,
      "status": "delivered",
      "sales_type": "web-sale-bodija",
      "shipment_creation_time": "2019-02-20 05:46:07",
      "shipment_delivery_time": "2019-02-20 05:46:07",
      "createdAt": "2019-02-20 05:46:07",
      "updatedAt": "2019-02-23 07:42:41"
    },
    "149": {
      "progression": [],
      "order": {
        "id": 124,
        "orderNo": "FH-0220054955",
        "order_creation_time": "2019-02-20 05:49:55",
        "order_delivery_time": "2019-02-20T10:49:55.000Z",
        "status": "delivered",
        "shipment": 149,
        "user": {
          "id": 30,
          "firstName": "Pasta",
          "lastName": "Lemon",
          "email": "",
          "phoneNo": "08023212123",
          "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
          "profilePix": "",
          "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-20 14:39:45",
          "last_visit_point_date": "2019-02-20 05:30:45",
          "foodCoins": 68,
          "cancelled_orders": 0,
          "referrer_code": "pasta_817",
          "referred_by": "jeremy337",
          "online": true,
          "createdAt": "2018-10-01T15:15:03.000Z",
          "updatedAt": "2019-02-20T19:39:45.000Z",
          "address": {
            "id": 15,
            "name": "Pls let's know",
            "description": "Pls let's know",
            "street": null,
            "google_map_street": "235 Ikorodu Rd, Ilupeju, Lagos, Nigeria",
            "latitude": "6.5538792",
            "longitude": "3.3662943",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 3,
              "name": "Akobo Baptist",
              "latitude": "7.432162",
              "longitude": "3.941496",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 275,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 1,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 600,
            "status": null
          }
        ]
      },
      "address": {
        "id": 150,
        "name": "Pls let's know",
        "description": "Pls let's know",
        "street": null,
        "google_map_street": "235 Ikorodu Rd, Ilupeju, Lagos, Nigeria",
        "latitude": "6.5538662",
        "longitude": "3.3662931",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 3,
          "name": "Akobo Baptist",
          "latitude": "7.432162",
          "longitude": "3.941496",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 30,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
        "profilePix": "",
        "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-20 14:39:45",
        "last_visit_point_date": "2019-02-20 05:30:45",
        "foodCoins": 68,
        "cancelled_orders": 0,
        "referrer_code": "pasta_817",
        "referred_by": "jeremy337",
        "online": true,
        "createdAt": "2018-10-01T15:15:03.000Z",
        "updatedAt": "2019-02-20T19:39:45.000Z",
        "address": 15
      },
      "guest": null,
      "deliverTo": {
        "id": 188,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 165,
        "orderCharge": "600",
        "deliveryCharge": "260",
        "amount": "860",
        "merchantRef": null,
        "transactionId": null,
        "status": "received",
        "createdAt": "2019-02-20T10:49:55.000Z",
        "updatedAt": "2019-02-20T11:27:10.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "deliveryAgent": {
        "id": 1,
        "firstName": "Adekunle",
        "lastName": "Akinola",
        "email": "olayinka@gmail.com",
        "phoneNo": "08024708088",
        "deviceId": "10049c4d-912d-4a5c-a385-35b70aab242e",
        "profilePix": "https://s3.amazonaws.com/foodhub-dev/general/default_avatar.jpg",
        "fcmId": "djh3woMeJK8:APA91bErBOyZ0Os8xYAvLFQyMNli-N1gGtONSD7lYV0dr1xc0nS8HPSGrqF_hPE1WvEEjB8IMuDCyIEj4HSEFLLvZSPXotrNAWYXP_3JHwEfxLPMtZMCXWl3KDma-4RSdvEQwMMbGwwD",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-14 02:48:57",
        "last_visit_point_date": "2019-02-14 02:48:57",
        "foodCoins": 42,
        "cancelled_orders": 4,
        "referrer_code": "olayinka_648",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-03T16:07:01.000Z",
        "updatedAt": "2019-02-26T08:30:47.000Z",
        "address": 2
      },
      "id": 149,
      "deliverycharge": "260",
      "latitude": "7.382728",
      "longitude": "3.382728",
      "eta": 40,
      "status": "delivered",
      "sales_type": "web-sale-bodija",
      "shipment_creation_time": "2019-02-20 05:49:55",
      "shipment_delivery_time": "2019-02-20 05:49:55",
      "createdAt": "2019-02-20 05:49:55",
      "updatedAt": "2019-02-20 06:27:29"
    },
    "150": {
      "progression": [],
      "order": {
        "id": 125,
        "orderNo": "FH-0220090025",
        "order_creation_time": "2019-02-20 09:00:25",
        "order_delivery_time": "2019-02-20T14:00:25.000Z",
        "status": "user-cancelled",
        "shipment": 150,
        "user": {
          "id": 30,
          "firstName": "Pasta",
          "lastName": "Lemon",
          "email": "",
          "phoneNo": "08023212123",
          "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
          "profilePix": "",
          "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-20 14:39:45",
          "last_visit_point_date": "2019-02-20 05:30:45",
          "foodCoins": 68,
          "cancelled_orders": 0,
          "referrer_code": "pasta_817",
          "referred_by": "jeremy337",
          "online": true,
          "createdAt": "2018-10-01T15:15:03.000Z",
          "updatedAt": "2019-02-20T19:39:45.000Z",
          "address": {
            "id": 15,
            "name": "Pls let's know",
            "description": "Pls let's know",
            "street": null,
            "google_map_street": "235 Ikorodu Rd, Ilupeju, Lagos, Nigeria",
            "latitude": "6.5538792",
            "longitude": "3.3662943",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 3,
              "name": "Akobo Baptist",
              "latitude": "7.432162",
              "longitude": "3.941496",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 276,
            "productvariant": {
              "id": 5,
              "name": "Garri (Yellow)",
              "short_description": "Yello Garri: Nice for Yellow Eba; contains vitamin A.",
              "description": "The yellow garri is a derivative of the white one which is achieved by mixing palm oil during the frying stage.",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/yellow_garri_2.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 15,
              "tags": "garri cassava eba fufu ijebu egba yellow",
              "category": 1,
              "product": 6
            },
            "quantity": 1,
            "measurement": {
              "id": 19,
              "cost_price": null,
              "per_unit_cost": 200,
              "old_per_unit_cost": 250,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/yellow_garri_2.jpg",
              "status": true,
              "productvariant": 5,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 200,
            "status": null
          }
        ]
      },
      "address": {
        "id": 151,
        "name": "Pls let's know",
        "description": "Pls let's know",
        "street": null,
        "google_map_street": "235 Ikorodu Rd, Ilupeju, Lagos, Nigeria",
        "latitude": "6.5538289",
        "longitude": "3.3662813999999996",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 3,
          "name": "Akobo Baptist",
          "latitude": "7.432162",
          "longitude": "3.941496",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 30,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
        "profilePix": "",
        "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-20 14:39:45",
        "last_visit_point_date": "2019-02-20 05:30:45",
        "foodCoins": 68,
        "cancelled_orders": 0,
        "referrer_code": "pasta_817",
        "referred_by": "jeremy337",
        "online": true,
        "createdAt": "2018-10-01T15:15:03.000Z",
        "updatedAt": "2019-02-20T19:39:45.000Z",
        "address": 15
      },
      "guest": null,
      "deliverTo": {
        "id": 189,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 166,
        "orderCharge": "200",
        "deliveryCharge": "260",
        "amount": "460",
        "merchantRef": null,
        "transactionId": null,
        "status": "not_yet_paid",
        "createdAt": "2019-02-20T14:00:25.000Z",
        "updatedAt": "2019-02-20T14:00:25.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "id": 150,
      "deliverycharge": "260",
      "latitude": "7.382728",
      "longitude": "3.382728",
      "eta": 50,
      "status": "user-cancelled",
      "sales_type": "web-sale-bodija",
      "shipment_creation_time": "2019-02-20 09:00:25",
      "shipment_delivery_time": "2019-02-20 09:00:25",
      "createdAt": "2019-02-20 09:00:25",
      "updatedAt": "2019-02-20 14:37:31"
    },
    "151": {
      "progression": [],
      "order": {
        "id": 126,
        "orderNo": "FH-0220090846",
        "order_creation_time": "2019-02-20 09:08:46",
        "order_delivery_time": "2019-02-20T14:08:46.000Z",
        "status": "delivered",
        "shipment": 151,
        "user": {
          "id": 30,
          "firstName": "Pasta",
          "lastName": "Lemon",
          "email": "",
          "phoneNo": "08023212123",
          "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
          "profilePix": "",
          "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-20 14:39:45",
          "last_visit_point_date": "2019-02-20 05:30:45",
          "foodCoins": 68,
          "cancelled_orders": 0,
          "referrer_code": "pasta_817",
          "referred_by": "jeremy337",
          "online": true,
          "createdAt": "2018-10-01T15:15:03.000Z",
          "updatedAt": "2019-02-20T19:39:45.000Z",
          "address": {
            "id": 15,
            "name": "Pls let's know",
            "description": "Pls let's know",
            "street": null,
            "google_map_street": "235 Ikorodu Rd, Ilupeju, Lagos, Nigeria",
            "latitude": "6.5538792",
            "longitude": "3.3662943",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 3,
              "name": "Akobo Baptist",
              "latitude": "7.432162",
              "longitude": "3.941496",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 277,
            "productvariant": {
              "id": 59,
              "name": "Chicken Laps",
              "short_description": "Big frozen chicken laps",
              "description": "Big frozen chicken laps",
              "price": 0,
              "image": "https://s3.amazonaws.com/foodhub-dev/dairy/chicken-laps-1.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 12,
              "tags": "chicken lap laps meat fowl broiler cockrel",
              "category": 6,
              "product": 12
            },
            "quantity": 1,
            "measurement": {
              "id": 137,
              "cost_price": 980,
              "per_unit_cost": 1050,
              "old_per_unit_cost": 1100,
              "image": "https://s3.amazonaws.com/foodhub-dev/dairy/chicken-laps-1.jpg",
              "status": true,
              "productvariant": 59,
              "scale": {
                "id": 62,
                "name": "Kilo",
                "description": "1Kg = 1 Kilo",
                "image": null,
                "status": true
              }
            },
            "price": 1050,
            "status": null
          }
        ]
      },
      "address": {
        "id": 152,
        "name": "Pls let's know",
        "description": "Pls let's know",
        "street": null,
        "google_map_street": "235 Ikorodu Rd, Ilupeju, Lagos, Nigeria",
        "latitude": "6.5538289",
        "longitude": "3.3662813999999996",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 3,
          "name": "Akobo Baptist",
          "latitude": "7.432162",
          "longitude": "3.941496",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 30,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
        "profilePix": "",
        "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-20 14:39:45",
        "last_visit_point_date": "2019-02-20 05:30:45",
        "foodCoins": 68,
        "cancelled_orders": 0,
        "referrer_code": "pasta_817",
        "referred_by": "jeremy337",
        "online": true,
        "createdAt": "2018-10-01T15:15:03.000Z",
        "updatedAt": "2019-02-20T19:39:45.000Z",
        "address": 15
      },
      "guest": null,
      "deliverTo": {
        "id": 190,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 167,
        "orderCharge": "1050",
        "deliveryCharge": "260",
        "amount": "1310",
        "merchantRef": null,
        "transactionId": null,
        "status": "received",
        "createdAt": "2019-02-20T14:08:46.000Z",
        "updatedAt": "2019-02-23T07:52:09.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "deliveryAgent": {
        "id": 1,
        "firstName": "Adekunle",
        "lastName": "Akinola",
        "email": "olayinka@gmail.com",
        "phoneNo": "08024708088",
        "deviceId": "10049c4d-912d-4a5c-a385-35b70aab242e",
        "profilePix": "https://s3.amazonaws.com/foodhub-dev/general/default_avatar.jpg",
        "fcmId": "djh3woMeJK8:APA91bErBOyZ0Os8xYAvLFQyMNli-N1gGtONSD7lYV0dr1xc0nS8HPSGrqF_hPE1WvEEjB8IMuDCyIEj4HSEFLLvZSPXotrNAWYXP_3JHwEfxLPMtZMCXWl3KDma-4RSdvEQwMMbGwwD",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-14 02:48:57",
        "last_visit_point_date": "2019-02-14 02:48:57",
        "foodCoins": 42,
        "cancelled_orders": 4,
        "referrer_code": "olayinka_648",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-03T16:07:01.000Z",
        "updatedAt": "2019-02-26T08:30:47.000Z",
        "address": 2
      },
      "id": 151,
      "deliverycharge": "260",
      "latitude": "7.382728",
      "longitude": "3.382728",
      "eta": 34,
      "status": "delivered",
      "sales_type": "web-sale-bodija",
      "shipment_creation_time": "2019-02-20 09:08:46",
      "shipment_delivery_time": "2019-02-20 09:08:46",
      "createdAt": "2019-02-20 09:08:46",
      "updatedAt": "2019-02-23 02:56:56"
    },
    "152": {
      "progression": [],
      "order": {
        "id": 127,
        "orderNo": "FH-0220143608",
        "order_creation_time": "2019-02-20 14:36:08",
        "order_delivery_time": "2019-02-20T19:36:08.000Z",
        "status": "delivered",
        "shipment": 152,
        "user": {
          "id": 30,
          "firstName": "Pasta",
          "lastName": "Lemon",
          "email": "",
          "phoneNo": "08023212123",
          "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
          "profilePix": "",
          "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-20 14:39:45",
          "last_visit_point_date": "2019-02-20 05:30:45",
          "foodCoins": 68,
          "cancelled_orders": 0,
          "referrer_code": "pasta_817",
          "referred_by": "jeremy337",
          "online": true,
          "createdAt": "2018-10-01T15:15:03.000Z",
          "updatedAt": "2019-02-20T19:39:45.000Z",
          "address": {
            "id": 15,
            "name": "Pls let's know",
            "description": "Pls let's know",
            "street": null,
            "google_map_street": "235 Ikorodu Rd, Ilupeju, Lagos, Nigeria",
            "latitude": "6.5538792",
            "longitude": "3.3662943",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 3,
              "name": "Akobo Baptist",
              "latitude": "7.432162",
              "longitude": "3.941496",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 278,
            "productvariant": {
              "id": 2,
              "name": "Honeywell Semolina",
              "short_description": "Tasty Semolina. Honeywell Brand",
              "description": "Tasty Semolina. Honeywell Brand",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/honeywell_semolina_2kg.png",
              "stockcount": 10,
              "status": true,
              "isAvailable": true,
              "askance_index": 23,
              "tags": "honeywell semolina semo semovita",
              "category": 1,
              "product": 4
            },
            "quantity": 1,
            "measurement": {
              "id": 9,
              "cost_price": 1450,
              "per_unit_cost": 1550,
              "old_per_unit_cost": 1700,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/honeywell_semolina_5kg.jpeg",
              "status": true,
              "productvariant": 2,
              "scale": {
                "id": 26,
                "name": "5Kg Bag",
                "description": null,
                "image": null,
                "status": null
              }
            },
            "price": 1550,
            "status": null
          },
          {
            "id": 279,
            "productvariant": {
              "id": 4,
              "name": "Golden Penny Semovita",
              "short_description": "Fine Semovita. Golden Penny Brand",
              "description": "Fine Semovita. Golden Penny Brand",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/golden_penny_semovita_1kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 6,
              "tags": "golden penny semovita semolina semo",
              "category": 1,
              "product": 5
            },
            "quantity": 1,
            "measurement": {
              "id": 17,
              "cost_price": 1450,
              "per_unit_cost": 1500,
              "old_per_unit_cost": 1700,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/golden_penny_semovita_5kg.jpg",
              "status": true,
              "productvariant": 4,
              "scale": {
                "id": 26,
                "name": "5Kg Bag",
                "description": null,
                "image": null,
                "status": null
              }
            },
            "price": 1500,
            "status": null
          },
          {
            "id": 280,
            "productvariant": {
              "id": 14,
              "name": "Fresh Beef (Cow Meat)",
              "short_description": "Fresh Beef (Cow Meat)",
              "description": "Fresh Beef (Cow Meat)",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/dairy/cow-tail-3.jpg",
              "stockcount": 0,
              "status": true,
              "isAvailable": true,
              "askance_index": 8,
              "tags": "cow meat eran malu beef ora isan",
              "category": 6,
              "product": 12
            },
            "quantity": 3,
            "measurement": {
              "id": 37,
              "cost_price": null,
              "per_unit_cost": 800,
              "old_per_unit_cost": 900,
              "image": "https://s3.amazonaws.com/foodhub-dev/dairy/cow_meat_2.jpg",
              "status": true,
              "productvariant": 14,
              "scale": {
                "id": 30,
                "name": "500grams",
                "description": null,
                "image": null,
                "status": true
              }
            },
            "price": 2400,
            "status": null
          }
        ]
      },
      "address": {
        "id": 153,
        "name": "Pls let's know",
        "description": "Pls let's know",
        "street": null,
        "google_map_street": "14 Adekoya St, Mafoluku Oshodi, Lagos, Nigeria",
        "latitude": "6.5601536",
        "longitude": "3.3480703999999997",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 3,
          "name": "Akobo Baptist",
          "latitude": "7.432162",
          "longitude": "3.941496",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 30,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "deviceId": "8fadeda6-fe5f-453e-a52a-e93da0688c80",
        "profilePix": "",
        "fcmId": "eKceDoMfHIQ:APA91bFZIhPkCeohBMJMcm5NS1CLsyWrYSIRx74DhE44thozwcp8x6ZYK9CPDXx_6XvWzayQQO9o646SUryjwj_QtHOgF-9qRWz-dYd3lWJjVVhna6RWTDfV7I8sMQb1lxLlVUhSvgWC",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-20 14:39:45",
        "last_visit_point_date": "2019-02-20 05:30:45",
        "foodCoins": 68,
        "cancelled_orders": 0,
        "referrer_code": "pasta_817",
        "referred_by": "jeremy337",
        "online": true,
        "createdAt": "2018-10-01T15:15:03.000Z",
        "updatedAt": "2019-02-20T19:39:45.000Z",
        "address": 15
      },
      "guest": null,
      "deliverTo": {
        "id": 191,
        "firstName": "Pasta",
        "lastName": "Lemon",
        "email": "",
        "phoneNo": "08023212123",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 168,
        "orderCharge": "5450",
        "deliveryCharge": "260",
        "amount": "5710",
        "merchantRef": null,
        "transactionId": null,
        "status": "received",
        "createdAt": "2019-02-20T19:36:07.000Z",
        "updatedAt": "2019-02-20T19:41:05.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "deliveryAgent": {
        "id": 2,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "deviceId": null,
        "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
        "fcmId": "",
        "facebookId": "10214898335554789",
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-18 06:41:57",
        "last_visit_point_date": "2019-02-18 02:01:07",
        "foodCoins": 103,
        "cancelled_orders": 2,
        "referrer_code": "akinola_447",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-06T20:55:06.000Z",
        "updatedAt": "2019-02-18T11:42:53.000Z",
        "address": 3
      },
      "id": 152,
      "deliverycharge": "260",
      "latitude": "7.382728",
      "longitude": "3.382728",
      "eta": 47,
      "status": "delivered",
      "sales_type": "web-sale-bodija",
      "shipment_creation_time": "2019-02-20 14:36:08",
      "shipment_delivery_time": "2019-02-20 14:36:08",
      "createdAt": "2019-02-20 14:36:08",
      "updatedAt": "2019-02-20 14:41:18"
    },
    "153": {
      "progression": [],
      "order": {
        "id": 128,
        "orderNo": "FH-0220151818",
        "order_creation_time": "2019-02-20 15:18:18",
        "order_delivery_time": "2019-02-20T20:18:18.000Z",
        "status": "delivered",
        "shipment": 153,
        "user": {
          "id": 7,
          "firstName": "Seni",
          "lastName": "Sulyman",
          "email": "olayinka@lawtrades.com",
          "phoneNo": "09023221019",
          "deviceId": null,
          "profilePix": null,
          "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-26 03:31:46",
          "last_visit_point_date": "2019-02-25 07:12:35",
          "foodCoins": 10,
          "cancelled_orders": 0,
          "referrer_code": "seni_290",
          "referred_by": "admin_993",
          "online": true,
          "createdAt": "2018-08-10T23:51:01.000Z",
          "updatedAt": "2019-02-26T08:31:47.000Z",
          "address": {
            "id": 16,
            "name": "Dunoddnd",
            "description": "Dunoddnd",
            "street": null,
            "google_map_street": "",
            "latitude": "0",
            "longitude": "0",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 7,
              "name": "Alegongo Primary",
              "latitude": "7.440793",
              "longitude": "3.951111",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 281,
            "productvariant": {
              "id": 43,
              "name": "Fresh Cabbage",
              "short_description": "It is used for making salad.",
              "description": "It is used for making salad.",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/vegetables/cabbage-1.jpg",
              "stockcount": 12,
              "status": true,
              "isAvailable": true,
              "askance_index": 2,
              "tags": "cabbage salad lettuce leaves",
              "category": 3,
              "product": 18
            },
            "quantity": 1,
            "measurement": {
              "id": 95,
              "cost_price": null,
              "per_unit_cost": 300,
              "old_per_unit_cost": 350,
              "image": "https://s3.amazonaws.com/foodhub-dev/vegetables/cabbage-1.jpg",
              "status": true,
              "productvariant": 43,
              "scale": {
                "id": 45,
                "name": "Medium Size",
                "description": null,
                "image": null,
                "status": null
              }
            },
            "price": 300,
            "status": null
          },
          {
            "id": 282,
            "productvariant": {
              "id": 4,
              "name": "Golden Penny Semovita",
              "short_description": "Fine Semovita. Golden Penny Brand",
              "description": "Fine Semovita. Golden Penny Brand",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/golden_penny_semovita_1kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 6,
              "tags": "golden penny semovita semolina semo",
              "category": 1,
              "product": 5
            },
            "quantity": 1,
            "measurement": {
              "id": 18,
              "cost_price": 2750,
              "per_unit_cost": 2900,
              "old_per_unit_cost": 3200,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/golden_penny_semovita_10kg_2.jpg",
              "status": true,
              "productvariant": 4,
              "scale": {
                "id": 27,
                "name": "10Kg Bag",
                "description": null,
                "image": null,
                "status": null
              }
            },
            "price": 2900,
            "status": null
          },
          {
            "id": 283,
            "productvariant": {
              "id": 771,
              "name": "Fresh Raw Bawa (Pepper)",
              "short_description": "Fresh Raw Bawa (Pepper)",
              "description": "Fresh Raw Bawa (Pepper)",
              "price": 0,
              "image": "https://s3.amazonaws.com/foodhub-dev/spices/sombo-2.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 5,
              "tags": "pepper ata sombo rodo tatase tatashe bawa",
              "category": 4,
              "product": 17
            },
            "quantity": 1,
            "measurement": {
              "id": 2171,
              "cost_price": 0,
              "per_unit_cost": 200,
              "old_per_unit_cost": 200,
              "image": "https://s3.amazonaws.com/foodhub-dev/spices/sombo-2.jpg",
              "status": true,
              "productvariant": 771,
              "scale": {
                "id": 42,
                "name": "Large Plate(Ele)",
                "description": null,
                "image": null,
                "status": null
              }
            },
            "price": 200,
            "status": null
          }
        ]
      },
      "address": {
        "id": 154,
        "name": "27 Ikosi Road, Ikosi Ketu, Lagos, Nigeria",
        "description": "Opposite Headmaster barber",
        "street": "",
        "google_map_street": "Lagos",
        "latitude": "6.5999183",
        "longitude": "3.3851583",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 5,
          "name": "Anifalaje Estate",
          "latitude": "7.435996",
          "longitude": "3.945762",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 7,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "deviceId": null,
        "profilePix": null,
        "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-26 03:31:46",
        "last_visit_point_date": "2019-02-25 07:12:35",
        "foodCoins": 10,
        "cancelled_orders": 0,
        "referrer_code": "seni_290",
        "referred_by": "admin_993",
        "online": true,
        "createdAt": "2018-08-10T23:51:01.000Z",
        "updatedAt": "2019-02-26T08:31:47.000Z",
        "address": 16
      },
      "guest": null,
      "deliverTo": {
        "id": 192,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 169,
        "orderCharge": "3400",
        "deliveryCharge": "290",
        "amount": "3690",
        "merchantRef": null,
        "transactionId": null,
        "status": "received",
        "createdAt": "2019-02-20T20:18:18.000Z",
        "updatedAt": "2019-02-20T20:52:57.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "deliveryAgent": {
        "id": 2,
        "firstName": "Akinola",
        "lastName": "Olayinko",
        "email": "liljoeengr22@yahoo.com",
        "phoneNo": "08024708087",
        "deviceId": null,
        "profilePix": "https://graph.facebook.com/10214898335554789/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D",
        "fcmId": "",
        "facebookId": "10214898335554789",
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-18 06:41:57",
        "last_visit_point_date": "2019-02-18 02:01:07",
        "foodCoins": 103,
        "cancelled_orders": 2,
        "referrer_code": "akinola_447",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-06T20:55:06.000Z",
        "updatedAt": "2019-02-18T11:42:53.000Z",
        "address": 3
      },
      "shop": {
        "id": 1,
        "name": "FoodHub Akobo",
        "latitude": "7.432162",
        "longitude": "3.941496",
        "status": true
      },
      "id": 153,
      "deliverycharge": "0",
      "latitude": "7.432162",
      "longitude": "3.941496",
      "eta": 26,
      "status": "delivered",
      "sales_type": "app-sale",
      "shipment_creation_time": "2019-02-20 15:18:18",
      "shipment_delivery_time": "2019-02-20 15:18:18",
      "createdAt": "2019-02-20 15:18:18",
      "updatedAt": "2019-02-20 15:55:19"
    },
    "154": {
      "progression": [],
      "order": {
        "id": 129,
        "orderNo": "FH-0220160129",
        "order_creation_time": "2019-02-20 16:01:29",
        "order_delivery_time": "2019-02-20T21:01:29.000Z",
        "status": "in-transit",
        "shipment": 154,
        "user": {
          "id": 7,
          "firstName": "Seni",
          "lastName": "Sulyman",
          "email": "olayinka@lawtrades.com",
          "phoneNo": "09023221019",
          "deviceId": null,
          "profilePix": null,
          "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-26 03:31:46",
          "last_visit_point_date": "2019-02-25 07:12:35",
          "foodCoins": 10,
          "cancelled_orders": 0,
          "referrer_code": "seni_290",
          "referred_by": "admin_993",
          "online": true,
          "createdAt": "2018-08-10T23:51:01.000Z",
          "updatedAt": "2019-02-26T08:31:47.000Z",
          "address": {
            "id": 16,
            "name": "Dunoddnd",
            "description": "Dunoddnd",
            "street": null,
            "google_map_street": "",
            "latitude": "0",
            "longitude": "0",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 7,
              "name": "Alegongo Primary",
              "latitude": "7.440793",
              "longitude": "3.951111",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 284,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 4,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 2400,
            "status": null
          }
        ]
      },
      "address": {
        "id": 155,
        "name": "27 Ikosi Road, Ikosi Ketu, Lagos, Nigeria",
        "description": "Opposite Headmaster barber",
        "street": "",
        "google_map_street": "Lagos",
        "latitude": "6.5999183",
        "longitude": "3.3851583",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 5,
          "name": "Anifalaje Estate",
          "latitude": "7.435996",
          "longitude": "3.945762",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 7,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "deviceId": null,
        "profilePix": null,
        "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-26 03:31:46",
        "last_visit_point_date": "2019-02-25 07:12:35",
        "foodCoins": 10,
        "cancelled_orders": 0,
        "referrer_code": "seni_290",
        "referred_by": "admin_993",
        "online": true,
        "createdAt": "2018-08-10T23:51:01.000Z",
        "updatedAt": "2019-02-26T08:31:47.000Z",
        "address": 16
      },
      "guest": null,
      "deliverTo": {
        "id": 193,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 170,
        "orderCharge": "2400",
        "deliveryCharge": "290",
        "amount": "2690",
        "merchantRef": null,
        "transactionId": null,
        "status": "not_yet_paid",
        "createdAt": "2019-02-20T21:01:29.000Z",
        "updatedAt": "2019-02-20T21:01:29.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "shop": {
        "id": 1,
        "name": "FoodHub Akobo",
        "latitude": "7.432162",
        "longitude": "3.941496",
        "status": true
      },
      "id": 154,
      "deliverycharge": "0",
      "latitude": "7.432162",
      "longitude": "3.941496",
      "eta": 0,
      "status": "in-transit",
      "sales_type": "app-sale",
      "shipment_creation_time": "2019-02-20 16:01:29",
      "shipment_delivery_time": "2019-02-20 16:01:29",
      "createdAt": "2019-02-20 16:01:29",
      "updatedAt": "2019-02-20 16:09:38"
    },
    "155": {
      "progression": [
        {
          "id": 11,
          "shipment": 155,
          "user": 1,
          "status": "created",
          "createdAt": "2019-02-26T07:55:41.000Z",
          "updatedAt": "2019-02-26T07:55:41.000Z"
        }
      ],
      "order": {
        "id": 130,
        "orderNo": "FH-0226025541",
        "order_creation_time": "2019-02-26 02:55:41",
        "order_delivery_time": "2019-02-26T07:55:41.000Z",
        "status": "picked-up",
        "shipment": 155,
        "user": {
          "id": 1,
          "firstName": "Adekunle",
          "lastName": "Akinola",
          "email": "olayinka@gmail.com",
          "phoneNo": "08024708088",
          "deviceId": "10049c4d-912d-4a5c-a385-35b70aab242e",
          "profilePix": "https://s3.amazonaws.com/foodhub-dev/general/default_avatar.jpg",
          "fcmId": "djh3woMeJK8:APA91bErBOyZ0Os8xYAvLFQyMNli-N1gGtONSD7lYV0dr1xc0nS8HPSGrqF_hPE1WvEEjB8IMuDCyIEj4HSEFLLvZSPXotrNAWYXP_3JHwEfxLPMtZMCXWl3KDma-4RSdvEQwMMbGwwD",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-14 02:48:57",
          "last_visit_point_date": "2019-02-14 02:48:57",
          "foodCoins": 42,
          "cancelled_orders": 4,
          "referrer_code": "olayinka_648",
          "referred_by": "",
          "online": true,
          "createdAt": "2018-01-03T16:07:01.000Z",
          "updatedAt": "2019-02-27T05:52:37.000Z",
          "address": {
            "id": 2,
            "name": "15 Olusoji Idowu Street, Lagos, Lagos, Nigeria",
            "description": "Akobo Baptist Junction, Opposite Headmaster Barbing Saloon.",
            "street": "",
            "google_map_street": "Olusoji Idowu Street",
            "latitude": "6.55165",
            "longitude": "3.3661533",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 9,
              "name": "Alao Akala Way",
              "latitude": "7.443527",
              "longitude": "3.951607",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 285,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 20,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 12000,
            "status": null
          }
        ]
      },
      "address": {
        "id": 156,
        "name": "15 Olusoji Idowu Street, Lagos, Lagos, Nigeria",
        "description": "Akobo Baptist Junction, Opposite Headmaster Barbing Saloon.",
        "street": "",
        "google_map_street": "Olusoji Idowu Street",
        "latitude": "6.55165",
        "longitude": "3.3661533",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 9,
          "name": "Alao Akala Way",
          "latitude": "7.443527",
          "longitude": "3.951607",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 1,
        "firstName": "Adekunle",
        "lastName": "Akinola",
        "email": "olayinka@gmail.com",
        "phoneNo": "08024708088",
        "deviceId": "10049c4d-912d-4a5c-a385-35b70aab242e",
        "profilePix": "https://s3.amazonaws.com/foodhub-dev/general/default_avatar.jpg",
        "fcmId": "djh3woMeJK8:APA91bErBOyZ0Os8xYAvLFQyMNli-N1gGtONSD7lYV0dr1xc0nS8HPSGrqF_hPE1WvEEjB8IMuDCyIEj4HSEFLLvZSPXotrNAWYXP_3JHwEfxLPMtZMCXWl3KDma-4RSdvEQwMMbGwwD",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-14 02:48:57",
        "last_visit_point_date": "2019-02-14 02:48:57",
        "foodCoins": 42,
        "cancelled_orders": 4,
        "referrer_code": "olayinka_648",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-03T16:07:01.000Z",
        "updatedAt": "2019-02-26T08:30:47.000Z",
        "address": 2
      },
      "guest": null,
      "deliverTo": {
        "id": 194,
        "firstName": "Adekunle",
        "lastName": "Akinola",
        "email": "olayinka@gmail.com",
        "phoneNo": "08024708088",
        "status": true
      },
      "deliverymode": {
        "id": 1,
        "name": "Store Pickup",
        "description": "Customer picks up purchase from the Store",
        "base_charge": 0,
        "cost_per_kilometer": 0,
        "free_threshold": null,
        "status": false
      },
      "payment": {
        "id": 171,
        "orderCharge": "12000",
        "deliveryCharge": "0",
        "amount": "12000",
        "merchantRef": null,
        "transactionId": null,
        "status": "received",
        "createdAt": "2019-02-26T07:55:41.000Z",
        "updatedAt": "2019-02-26T07:55:41.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "shop": {
        "id": 1,
        "name": "FoodHub Akobo",
        "latitude": "7.432162",
        "longitude": "3.941496",
        "status": true
      },
      "id": 155,
      "deliverycharge": "0",
      "latitude": "6.55165",
      "longitude": "3.3661533",
      "eta": 30,
      "status": "picked-up",
      "sales_type": "store-sale",
      "shipment_creation_time": "2019-02-26 02:55:41",
      "shipment_delivery_time": "2019-02-26 02:55:41",
      "createdAt": "2019-02-26 02:55:41",
      "updatedAt": "2019-02-26 02:55:41"
    },
    "156": {
      "progression": [
        {
          "id": 12,
          "shipment": 156,
          "user": 7,
          "status": "active",
          "createdAt": "2019-02-26T08:16:57.000Z",
          "updatedAt": "2019-02-26T08:16:57.000Z"
        }
      ],
      "order": {
        "id": 131,
        "orderNo": "FH-0226031657",
        "order_creation_time": "2019-02-26 03:16:57",
        "order_delivery_time": "2019-02-26T08:16:57.000Z",
        "status": "created",
        "shipment": 156,
        "user": {
          "id": 7,
          "firstName": "Seni",
          "lastName": "Sulyman",
          "email": "olayinka@lawtrades.com",
          "phoneNo": "09023221019",
          "deviceId": null,
          "profilePix": null,
          "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-26 03:31:46",
          "last_visit_point_date": "2019-02-25 07:12:35",
          "foodCoins": 10,
          "cancelled_orders": 0,
          "referrer_code": "seni_290",
          "referred_by": "admin_993",
          "online": true,
          "createdAt": "2018-08-10T23:51:01.000Z",
          "updatedAt": "2019-02-26T08:31:47.000Z",
          "address": {
            "id": 16,
            "name": "Dunoddnd",
            "description": "Dunoddnd",
            "street": null,
            "google_map_street": "",
            "latitude": "0",
            "longitude": "0",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 7,
              "name": "Alegongo Primary",
              "latitude": "7.440793",
              "longitude": "3.951111",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 286,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 1,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 600,
            "status": null
          }
        ]
      },
      "address": {
        "id": 157,
        "name": "Dunukofia Street",
        "description": "Dunukofia Street",
        "street": null,
        "google_map_street": "",
        "latitude": "0",
        "longitude": "0",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 12,
          "name": "Alafia Estate",
          "latitude": "7.4582963",
          "longitude": "3.9506882",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 7,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "deviceId": null,
        "profilePix": null,
        "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-26 03:31:46",
        "last_visit_point_date": "2019-02-25 07:12:35",
        "foodCoins": 10,
        "cancelled_orders": 0,
        "referrer_code": "seni_290",
        "referred_by": "admin_993",
        "online": true,
        "createdAt": "2018-08-10T23:51:01.000Z",
        "updatedAt": "2019-02-26T08:31:47.000Z",
        "address": 16
      },
      "guest": null,
      "deliverTo": {
        "id": 195,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 172,
        "orderCharge": "600",
        "deliveryCharge": "350",
        "amount": "950",
        "merchantRef": null,
        "transactionId": null,
        "status": "not_yet_paid",
        "createdAt": "2019-02-26T08:16:57.000Z",
        "updatedAt": "2019-02-26T08:16:57.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "id": 156,
      "deliverycharge": "350",
      "latitude": "7.382728",
      "longitude": "3.382728",
      "eta": 60,
      "status": "active",
      "sales_type": "web-sale-bodija",
      "shipment_creation_time": "2019-02-26 03:16:57",
      "shipment_delivery_time": "2019-02-26 03:16:57",
      "createdAt": "2019-02-26 03:16:57",
      "updatedAt": "2019-02-26 03:16:57"
    },
    "157": {
      "progression": [
        {
          "id": 13,
          "shipment": 157,
          "user": 7,
          "status": "active",
          "createdAt": "2019-02-26T08:22:43.000Z",
          "updatedAt": "2019-02-26T08:22:43.000Z"
        }
      ],
      "order": {
        "id": 132,
        "orderNo": "FH-0226032243",
        "order_creation_time": "2019-02-26 03:22:43",
        "order_delivery_time": "2019-02-26T08:22:43.000Z",
        "status": "created",
        "shipment": 157,
        "user": {
          "id": 7,
          "firstName": "Seni",
          "lastName": "Sulyman",
          "email": "olayinka@lawtrades.com",
          "phoneNo": "09023221019",
          "deviceId": null,
          "profilePix": null,
          "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-26 03:31:46",
          "last_visit_point_date": "2019-02-25 07:12:35",
          "foodCoins": 10,
          "cancelled_orders": 0,
          "referrer_code": "seni_290",
          "referred_by": "admin_993",
          "online": true,
          "createdAt": "2018-08-10T23:51:01.000Z",
          "updatedAt": "2019-02-26T08:31:47.000Z",
          "address": {
            "id": 16,
            "name": "Dunoddnd",
            "description": "Dunoddnd",
            "street": null,
            "google_map_street": "",
            "latitude": "0",
            "longitude": "0",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 7,
              "name": "Alegongo Primary",
              "latitude": "7.440793",
              "longitude": "3.951111",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 287,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 1,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 600,
            "status": null
          }
        ]
      },
      "address": {
        "id": 158,
        "name": "Dunoddnd",
        "description": "Dunoddnd",
        "street": null,
        "google_map_street": "",
        "latitude": "0",
        "longitude": "0",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 7,
          "name": "Alegongo Primary",
          "latitude": "7.440793",
          "longitude": "3.951111",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 7,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "deviceId": null,
        "profilePix": null,
        "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-26 03:31:46",
        "last_visit_point_date": "2019-02-25 07:12:35",
        "foodCoins": 10,
        "cancelled_orders": 0,
        "referrer_code": "seni_290",
        "referred_by": "admin_993",
        "online": true,
        "createdAt": "2018-08-10T23:51:01.000Z",
        "updatedAt": "2019-02-26T08:31:47.000Z",
        "address": 16
      },
      "guest": null,
      "deliverTo": {
        "id": 196,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 173,
        "orderCharge": "600",
        "deliveryCharge": "320",
        "amount": "920",
        "merchantRef": null,
        "transactionId": null,
        "status": "not_yet_paid",
        "createdAt": "2019-02-26T08:22:43.000Z",
        "updatedAt": "2019-02-26T08:22:43.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "id": 157,
      "deliverycharge": "320",
      "latitude": "7.382728",
      "longitude": "3.382728",
      "eta": 55,
      "status": "active",
      "sales_type": "web-sale-bodija",
      "shipment_creation_time": "2019-02-26 03:22:43",
      "shipment_delivery_time": "2019-02-26 03:22:43",
      "createdAt": "2019-02-26 03:22:43",
      "updatedAt": "2019-02-26 03:22:43"
    },
    "158": {
      "progression": [
        {
          "id": 14,
          "shipment": 158,
          "user": 7,
          "status": "active",
          "createdAt": "2019-02-26T08:24:49.000Z",
          "updatedAt": "2019-02-26T08:24:49.000Z"
        },
        {
          "id": 16,
          "shipment": 158,
          "user": 1,
          "status": "processing",
          "createdAt": "2019-02-26T08:30:36.000Z",
          "updatedAt": "2019-02-26T08:30:36.000Z"
        }
      ],
      "order": {
        "id": 133,
        "orderNo": "FH-0226032449",
        "order_creation_time": "2019-02-26 03:24:49",
        "order_delivery_time": "2019-02-26T08:24:49.000Z",
        "status": "processing",
        "shipment": 158,
        "user": {
          "id": 7,
          "firstName": "Seni",
          "lastName": "Sulyman",
          "email": "olayinka@lawtrades.com",
          "phoneNo": "09023221019",
          "deviceId": null,
          "profilePix": null,
          "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-26 03:31:46",
          "last_visit_point_date": "2019-02-25 07:12:35",
          "foodCoins": 10,
          "cancelled_orders": 0,
          "referrer_code": "seni_290",
          "referred_by": "admin_993",
          "online": true,
          "createdAt": "2018-08-10T23:51:01.000Z",
          "updatedAt": "2019-02-26T08:31:47.000Z",
          "address": {
            "id": 16,
            "name": "Dunoddnd",
            "description": "Dunoddnd",
            "street": null,
            "google_map_street": "",
            "latitude": "0",
            "longitude": "0",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 7,
              "name": "Alegongo Primary",
              "latitude": "7.440793",
              "longitude": "3.951111",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 288,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 1,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 600,
            "status": null
          }
        ]
      },
      "address": {
        "id": 159,
        "name": "Dunoddnd",
        "description": "Dunoddnd",
        "street": null,
        "google_map_street": "",
        "latitude": "0",
        "longitude": "0",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 7,
          "name": "Alegongo Primary",
          "latitude": "7.440793",
          "longitude": "3.951111",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 7,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "deviceId": null,
        "profilePix": null,
        "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-26 03:31:46",
        "last_visit_point_date": "2019-02-25 07:12:35",
        "foodCoins": 10,
        "cancelled_orders": 0,
        "referrer_code": "seni_290",
        "referred_by": "admin_993",
        "online": true,
        "createdAt": "2018-08-10T23:51:01.000Z",
        "updatedAt": "2019-02-26T08:31:47.000Z",
        "address": 16
      },
      "guest": null,
      "deliverTo": {
        "id": 197,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 174,
        "orderCharge": "600",
        "deliveryCharge": "320",
        "amount": "920",
        "merchantRef": null,
        "transactionId": null,
        "status": "not_yet_paid",
        "createdAt": "2019-02-26T08:24:49.000Z",
        "updatedAt": "2019-02-26T08:24:49.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "id": 158,
      "deliverycharge": "320",
      "latitude": "7.382728",
      "longitude": "3.382728",
      "eta": 0,
      "status": "processing",
      "sales_type": "web-sale-bodija",
      "shipment_creation_time": "2019-02-26 03:24:49",
      "shipment_delivery_time": "2019-02-26 03:24:49",
      "createdAt": "2019-02-26 03:24:49",
      "updatedAt": "2019-02-26 03:30:36"
    },
    "159": {
      "progression": [
        {
          "id": 15,
          "shipment": 159,
          "user": 7,
          "status": "active",
          "createdAt": "2019-02-26T08:28:32.000Z",
          "updatedAt": "2019-02-26T08:28:32.000Z"
        },
        {
          "id": 17,
          "shipment": 159,
          "user": 1,
          "status": "processing",
          "createdAt": "2019-02-26T08:30:57.000Z",
          "updatedAt": "2019-02-26T08:30:57.000Z"
        },
        {
          "id": 18,
          "shipment": 159,
          "user": 1,
          "status": "in-transit",
          "createdAt": "2019-02-26T08:32:35.000Z",
          "updatedAt": "2019-02-26T08:32:35.000Z"
        },
        {
          "id": 19,
          "shipment": 159,
          "user": 1,
          "status": "order-arrived",
          "createdAt": "2019-02-26T08:42:07.000Z",
          "updatedAt": "2019-02-26T08:42:07.000Z"
        },
        {
          "id": 20,
          "shipment": 159,
          "user": 1,
          "status": "payment-received",
          "createdAt": "2019-02-26T08:43:11.000Z",
          "updatedAt": "2019-02-26T08:43:11.000Z"
        },
        {
          "id": 21,
          "shipment": 159,
          "user": 1,
          "status": "delivered",
          "createdAt": "2019-02-26T08:44:15.000Z",
          "updatedAt": "2019-02-26T08:44:15.000Z"
        }
      ],
      "order": {
        "id": 134,
        "orderNo": "FH-0226032832",
        "order_creation_time": "2019-02-26 03:28:32",
        "order_delivery_time": "2019-02-26T08:28:32.000Z",
        "status": "delivered",
        "shipment": 159,
        "user": {
          "id": 7,
          "firstName": "Seni",
          "lastName": "Sulyman",
          "email": "olayinka@lawtrades.com",
          "phoneNo": "09023221019",
          "deviceId": null,
          "profilePix": null,
          "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
          "facebookId": null,
          "guestId": null,
          "status": true,
          "last_visit": "2019-02-26 03:31:46",
          "last_visit_point_date": "2019-02-25 07:12:35",
          "foodCoins": 10,
          "cancelled_orders": 0,
          "referrer_code": "seni_290",
          "referred_by": "admin_993",
          "online": true,
          "createdAt": "2018-08-10T23:51:01.000Z",
          "updatedAt": "2019-02-26T08:31:47.000Z",
          "address": {
            "id": 16,
            "name": "Dunoddnd",
            "description": "Dunoddnd",
            "street": null,
            "google_map_street": "",
            "latitude": "0",
            "longitude": "0",
            "state": {
              "id": 1,
              "name": "Oyo State",
              "status": true
            },
            "city": {
              "id": 1,
              "name": "Ibadan",
              "status": true,
              "state": 1
            },
            "district": {
              "id": 1,
              "name": "Akobo",
              "status": true,
              "createdAt": null,
              "updatedAt": "2018-04-28T17:37:09.000Z",
              "city": 1
            },
            "area": {
              "id": 7,
              "name": "Alegongo Primary",
              "latitude": "7.440793",
              "longitude": "3.951111",
              "status": true,
              "district": 1,
              "shop": {
                "id": 1,
                "name": "FoodHub Akobo",
                "latitude": "7.432162",
                "longitude": "3.941496",
                "status": true
              }
            }
          }
        },
        "suborders": [
          {
            "id": 289,
            "productvariant": {
              "id": 1,
              "name": "Rice Olomo N'la",
              "short_description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "description": "This rice has big seeds. Also known as Olomo N'la. It is good for Jollof Rice and Fried Rice",
              "price": null,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice_mama_gold_50kg.jpg",
              "stockcount": 100,
              "status": true,
              "isAvailable": true,
              "askance_index": 33,
              "tags": "rice mama gold big seed olomo nla ofada",
              "category": 1,
              "product": 1
            },
            "quantity": 1,
            "measurement": {
              "id": 1,
              "cost_price": 460,
              "per_unit_cost": 600,
              "old_per_unit_cost": 650,
              "image": "https://s3.amazonaws.com/foodhub-dev/grains/rice.jpg",
              "status": true,
              "productvariant": 1,
              "scale": {
                "id": 2,
                "name": "Congo",
                "description": "",
                "image": "",
                "status": false
              }
            },
            "price": 600,
            "status": null
          }
        ]
      },
      "address": {
        "id": 160,
        "name": "Dunoddnd",
        "description": "Dunoddnd",
        "street": null,
        "google_map_street": "",
        "latitude": "0",
        "longitude": "0",
        "state": {
          "id": 1,
          "name": "Oyo State",
          "status": true
        },
        "city": {
          "id": 1,
          "name": "Ibadan",
          "status": true,
          "state": 1
        },
        "district": {
          "id": 1,
          "name": "Akobo",
          "status": true,
          "createdAt": null,
          "updatedAt": "2018-04-28T17:37:09.000Z",
          "city": 1
        },
        "area": {
          "id": 7,
          "name": "Alegongo Primary",
          "latitude": "7.440793",
          "longitude": "3.951111",
          "status": true,
          "district": 1,
          "shop": {
            "id": 1,
            "name": "FoodHub Akobo",
            "latitude": "7.432162",
            "longitude": "3.941496",
            "status": true
          }
        }
      },
      "user": {
        "id": 7,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "deviceId": null,
        "profilePix": null,
        "fcmId": "eN6S960c_g4:APA91bGG_zXHYDSBh8bra_9ZVrscf6us7uWBFmSZiOqef6lPiAFnI6St7-J86xOvVmEnR9hXQdx8twNh0zQWfFyHf752K1F-bNzVm5yDC1mVp-qsrBiBQ5WM7_eQ296MCZyb8T6G-U2i",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-26 03:31:46",
        "last_visit_point_date": "2019-02-25 07:12:35",
        "foodCoins": 10,
        "cancelled_orders": 0,
        "referrer_code": "seni_290",
        "referred_by": "admin_993",
        "online": true,
        "createdAt": "2018-08-10T23:51:01.000Z",
        "updatedAt": "2019-02-26T08:31:47.000Z",
        "address": 16
      },
      "guest": null,
      "deliverTo": {
        "id": 198,
        "firstName": "Seni",
        "lastName": "Sulyman",
        "email": "olayinka@lawtrades.com",
        "phoneNo": "09023221019",
        "status": true
      },
      "deliverymode": {
        "id": 2,
        "name": "Home Delivery",
        "description": "Our delivery agents will deliver your product to your door step",
        "base_charge": 100,
        "cost_per_kilometer": 45,
        "free_threshold": 1000,
        "status": true
      },
      "payment": {
        "id": 175,
        "orderCharge": "600",
        "deliveryCharge": "320",
        "amount": "920",
        "merchantRef": null,
        "transactionId": null,
        "status": "received",
        "createdAt": "2019-02-26T08:28:32.000Z",
        "updatedAt": "2019-02-26T08:32:11.000Z",
        "mode": {
          "id": 1,
          "name": "Pay On Delivery (Cash)",
          "description": "Customer pays cash on Delivery of Goods.",
          "isDelivery": true,
          "isShopPickUp": false,
          "status": true
        },
        "shop": null
      },
      "deliveryAgent": {
        "id": 1,
        "firstName": "Adekunle",
        "lastName": "Akinola",
        "email": "olayinka@gmail.com",
        "phoneNo": "08024708088",
        "deviceId": "10049c4d-912d-4a5c-a385-35b70aab242e",
        "profilePix": "https://s3.amazonaws.com/foodhub-dev/general/default_avatar.jpg",
        "fcmId": "djh3woMeJK8:APA91bErBOyZ0Os8xYAvLFQyMNli-N1gGtONSD7lYV0dr1xc0nS8HPSGrqF_hPE1WvEEjB8IMuDCyIEj4HSEFLLvZSPXotrNAWYXP_3JHwEfxLPMtZMCXWl3KDma-4RSdvEQwMMbGwwD",
        "facebookId": null,
        "guestId": null,
        "status": true,
        "last_visit": "2019-02-14 02:48:57",
        "last_visit_point_date": "2019-02-14 02:48:57",
        "foodCoins": 42,
        "cancelled_orders": 4,
        "referrer_code": "olayinka_648",
        "referred_by": "",
        "online": true,
        "createdAt": "2018-01-03T16:07:01.000Z",
        "updatedAt": "2019-02-26T08:30:47.000Z",
        "address": 2
      },
      "id": 159,
      "deliverycharge": "320",
      "latitude": "7.382728",
      "longitude": "3.382728",
      "eta": 53,
      "status": "delivered",
      "sales_type": "web-sale-bodija",
      "shipment_creation_time": "2019-02-26 03:28:32",
      "shipment_delivery_time": "2019-02-26 03:28:32",
      "createdAt": "2019-02-26 03:28:32",
      "updatedAt": "2019-02-26 03:32:15"
    }
  }
};

export default shipment();
