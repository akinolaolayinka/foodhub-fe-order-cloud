import {HTTP} from './http-commons';
import {HTTP_BEARER} from './http-commons';


export default  {

  getShipments : function (data, callback) {
    HTTP_BEARER().get('/v2/admin/shipments?startDate=2019-08-01')
      .then(function (response) {
        callback(null, response.data.data);
      })
      .catch(function (err) {
        callback(err);
      })
  },

  createShipment : function (data, callback) {
    HTTP_BEARER().post(`/v2/admin/shipments`, data)
      .then(function (response) {
        console.log(response);
        callback(null, response.data.data);
      })
      .catch(function (err) {
        callback(err);
      })
  },


  updateShipmentStatus : function (data, callback) {
    HTTP_BEARER().put(`/v2/admin/shipments/${data.shipmentId}/updatestatus`, data)
      .then(function (response) {
        console.log(response.data.data);
        callback(null, response.data.data);
      })
      .catch(function (err) {
        callback(err);
      })
  },


  assignShipmentForDelivery : function (data, callback) {
    HTTP_BEARER().put(`/v2/admin/shipments/${data.shipmentId}/delivery/assign`, data)
      .then(function (response) {
        console.log(response.data.data);
        callback(null, response.data.data);
      })
      .catch(function (err) {
        callback(err);
      })
  },


}
