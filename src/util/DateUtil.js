import moment from 'moment';

export const listDaysBetweenTwoInstances = (start, end, format = 'YYYY-MM-DD', inclusive = true)=>{
  const dates = [];
  const startDate = moment(start, format).startOf('day');
  const endDate = moment(end, format).startOf('day');

  if (inclusive){
    dates.push(startDate.clone().format('YYYY-MM-DD'));
  }

  while(startDate.add(1, 'days').diff(endDate) <= 0) {
    dates.push(startDate.clone().format('YYYY-MM-DD'));
  }

  return dates;
};
