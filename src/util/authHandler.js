import {HTTP} from './http-commons';

export default  {

  login : function (data, callback) {
    HTTP.post('admin/login', data)
      .then(function (response) {
        callback(null, response.data);
      })
      .catch(function (err) {
        callback(err);
      })
  },


}
