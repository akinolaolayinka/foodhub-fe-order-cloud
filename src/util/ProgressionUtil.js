const {groupBy, each, keyBy, map, meanBy, sumBy} = require('lodash');
import moment from 'moment';

import shipments from './mock/shipments';
//let shipments = require('./mock/shipments');
//console.log(shipments);
let newShipments = [];
let defaultProgressionSummary = {
  timeToDiscover : 0,
  timeToPackage : 0,
  timeToArrive : 0,
  totalShipmentTime : 0
};

export const computeProgressionSummaryOfAllShipments = (shipments) => {
  each(shipments, shipment => {
    shipment.summary = computeProgressionSummary(shipment);
  });
  return shipments;
};

export const convertShipmentsToArray = (shipments) => {
  return Object.values(shipments);
};


export const groupShipmentsByDate = (shipments) => {
  each(shipments, shipment => {
    shipment.day = moment(shipment.createdAt, 'YYYY-MM-DD hh:mm:ss').format('YYYY-MM-DD');
  });

  return groupBy(shipments, 'day');
};


const computeProgressionSummary = (shipment) => {
  let progression = shipment.progression;
  let keyedProgression = keyBy(progression, 'status');
  let summary = {...defaultProgressionSummary};
  summary.timeToDiscover = computeTimeBetweenTwoStatuses(keyedProgression, 'active', 'processing');
  summary.timeToPackage = computeTimeBetweenTwoStatuses(keyedProgression, 'processing', 'in-transit');
  summary.timeToArrive = computeTimeBetweenTwoStatuses(keyedProgression, 'in-transit', 'delivered');
  summary.totalShipmentTime = computeTimeBetweenTwoStatuses(keyedProgression, 'active', 'delivered');
  return summary;
};


const computeTimeBetweenTwoStatuses = (keyedProgression, status1, status2) => {
  if (keyedProgression[status1] && keyedProgression[status2]) {
    return estimateTimeElapsed(keyedProgression[status1].updatedAt, keyedProgression[status2].updatedAt);
  } else {
    return 0;
  }
};


const estimateTimeElapsed = (timeStart, timeEnd) => {
  return Math.abs(moment(timeEnd).diff(moment(timeStart), 'minutes'));
};

export const evaluateDaysPerformance = (day, groupedShipments) => {
  const dayShipments = groupedShipments[day];
  const summaries = map(dayShipments, 'summary');
  let dayPerformance;
  if (summaries.length === 0) {
    dayPerformance = {
      timeToDiscover : 0,
      timeToPackage : 0,
      timeToArrive : 0,
      totalShipmentTime : 0,
    };
  } else {
    dayPerformance = {
      timeToDiscover : meanBy(summaries, 'timeToDiscover'),
      timeToPackage : meanBy(summaries, 'timeToPackage'),
      timeToArrive : meanBy(summaries, 'timeToArrive'),
      totalShipmentTime : meanBy(summaries, 'totalShipmentTime'),
    }
  }
  return dayPerformance;
};


/*computeProgressionSummaryOfAllShipments();

convertShipmentsToArray();


groupShipmentsByDate();


//computeProgressionSummary(shipments[159]);

evaluateDaysPerformance('2019-02-26');


listDaysBetweenTwoInstances('2019-02-01','2019-02-10');*/
