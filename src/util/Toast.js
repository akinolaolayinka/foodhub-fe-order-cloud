export default {
  Show : function (message = 'Error!', duration = 7000, style = '') {
    return M.toast({html : message, displayLength : duration, classes : style})
  },

  SUCCESS : function (message = 'Success!', duration = 7000, style = '') {
    return M.toast({
      html : message,
      displayLength : duration,
      classes : `teal ${style}`
    })
  },

  INFO : function (message = 'Success!', duration = 7000, style = '') {
    return M.toast({
      html : message,
      displayLength : duration,
      classes : `blue ${style}`
    })
  },

  ERROR : function (message = 'Error!', duration = 7000, style = 'rounded') {
    return M.toast({
      html : message,
      displayLength : duration,
      classes : `#e53935 red darken-1 white-text ${style}`
    })
  }

}
