import {HTTP, HTTP_BEARER} from './http-commons';


export default  {

  createUser: function (data, callback) {
    HTTP.post('/v1/auth/register', data)
      .then(function (response) {
        callback(null, response.data);
      })
      .catch(function (err) {
        callback(err);
      })
  },

  getMe: function (data, callback) {
    HTTP_BEARER().get('/v1/user/me')
      .then(function (response) {
        callback(null, response.data.data);
      })
      .catch(function (err) {
        console.log(err);
        callback(err);
      })
  },

  updateUserProfile: function (data, callback) {
    HTTP_BEARER().put('/v2/auth/update', data)
      .then(function (response) {
        callback(null, response.data);
      })
      .catch(function (err) {
        callback(err);
      })
  },

  createUserAddress: async (data)=> {
    try {
      let response = await HTTP_BEARER().post('/v1/address', data);
      return Promise.resolve(response.data.data);
    } catch (e) {
      console.log(e);
      return Promise.reject(e);
    };
  },

  setUserOffline: async ()=> {
    try {
      let response = await HTTP_BEARER().post('/v1/chat/client/unsubscribe');
      return Promise.resolve(response.data.data);
    } catch (e) {
      console.log(e);
      return Promise.reject(e);
    };
  },

  updateUserFCMId: function (data, callback) {
    HTTP_BEARER().put('/v1/user/updatefcmid', data)
      .then(function (response) {
        callback(null, response.data);
      })
      .catch(function (err) {
        console.log(err);
        callback(err);
      })
  }

}
