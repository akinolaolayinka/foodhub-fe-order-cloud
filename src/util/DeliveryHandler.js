import {HTTP} from './http-commons';
import {HTTP_BEARER} from './http-commons';


export default  {

  getDeliveryAgents : function (data, callback) {
    HTTP_BEARER().get('/v1/admin/delivery-agents')
      .then(function (response) {
        callback(null, response.data);
      })
      .catch(function (err) {
        console.log(err.response);
      })
  },




}
