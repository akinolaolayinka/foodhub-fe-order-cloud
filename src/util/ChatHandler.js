import {HTTP_BEARER} from "./http-commons";

export const getCustomers = async() => {
  let response = await HTTP_BEARER().get('/v1/users/online');
  return Promise.resolve(response.data.data);
};


export const getChats = (payload)=>{
  return HTTP_BEARER().get(`/v1/chat/client/${payload.id}/messages`)
    .then(function (response) {
      return Promise.resolve(response.data);
    })
    .catch(function (err) {
      console.log(err);
      return Promise.reject(err);
    })
};
