import Toast from './Toast';
import MapHelper from './MapHelper';
import moment from 'moment';

const Score = {
  'processing' : {
    score : 0,
    user : 0
  },
  'in-transit' : {
    score : 0,
    user : 0
  },
  'order-arrived' : {
    score : 0,
    user : 0
  },
  'delivered' : {
    score : 0,
    user : 0
  }
};

module.exports = {

  evaluatePerformance : (performanceArray) => {
    let performanceMap = MapHelper.ConvertArrayToMap(performanceArray, 'status');
    Score.processing = ComputeScore(performanceMap, 'created', 'processing', 6);
    console.log(Score);

    return Score;
  }
};

/**
 * @desc Evaluates the response to the advancement of a shipment between 2 statuses
 * @param performanceMap : performance map of a shipment
 * @param statusInit : initial status
 * @param statusFinal: final status
 * @param time: Acceptable time elapsed (in mins)
 * @returns {number}
 * @constructor
 */
function ComputeScore(performanceMap, statusInit, statusFinal, time = 5) {
  if (!performanceMap) {
    return {
      score: 0,
      user: 0
    };
  }

  if (!performanceMap[statusInit] || !performanceMap[statusFinal]) {
    return {
      score: 0,
      user: 0
    };
  }

  const timeInSecs = time * 60;
  const upperBound = 2.5 * timeInSecs;
  let startTime = moment(performanceMap[statusInit].createdAt);
  let endTime = moment(performanceMap[statusFinal].createdAt);

  let diff = endTime.diff(startTime, 's');
  let score = (upperBound - diff) / 4.5;

  if (score > 100) {
    return {
      score: 100,
      user: performanceMap[statusFinal].user
    };
  }

  if (score < 0) {
    return {
      score: 0,
      user: performanceMap[statusFinal].user
    };
  }


  return {
    score: score,
    user: performanceMap[statusFinal].user
  };
}
