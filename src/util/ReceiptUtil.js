module.exports = {
  GenerateReceipt : (data) => {
    let suborders = data.shipment.order.suborders;
    let shipment = data.shipment;


    let Laz = "&nbsp;".repeat(32);
    let L0 = pad("FoodHub", 32, "&nbsp;", 3);
    let L1 = "-".padStart(32, "-");
    let L2 = pad(shipment.updatedAt, 32, "&nbsp;", 2);
    let L2A = pad("Order No: " + shipment.order.orderNo, 32, "&nbsp;", 2);
    //let L2 = "2018-12-03".padEnd(19, "&nbsp;") + "FH-0129112920";
    let L3 = "=".padStart(32, "=");


    let out = `<span>${Laz}</span><br/>
                <span>${L0}</span><br/>
                <span>${L1}</span><br/>
                <span>${L2}</span><br/>
                <span>${L2A}</span><br/>
                <span>${L3}</span><br/>`;


    suborders.forEach((suborder, i) => {
      let name = "";
      if (suborder.measurement.scale.name.length > 10) {
        name = suborder.quantity + " ... of " + suborder.productvariant.name;
      } else {
        name = suborder.quantity + " " + suborder.measurement.scale.name + " of " + suborder.productvariant.name;
      }
      let l1 = name.length;
      if (l1 > 20) {
        name = name.substr(0, 20) + '.';
      } else {
        name = pad(name, 20, "&nbsp;", 2);
      }
      //let l2 = suborder.cost.length;
      //let lengthSum = l1 + l2;
      let c = "&#8358;" + suborder.price;
      let LX = name + pad(c, 17, "&nbsp;", 1);
      //let LX = name.padEnd(32 - 5, "-") + "&#8358;" + order.cost;
      //console.log(LX);
      out += `<span>${LX}</span><br/>`;
    });
    let LU = "&nbsp;".repeat(32);
    let dcharge = "&#8358;" + shipment.payment.deliveryCharge
    let LUA = "Delivery Charge: " + pad(dcharge, 21, "&nbsp;", 1);
    let LV = "&nbsp;".repeat(32);
    let L4 = "=".padStart(32, "=");
    let total = "&#8358;" + shipment.payment.amount;
    let L5 = "Total" + pad(total, 33, "&nbsp;", 1);
    let LW = "=".padStart(32, "=");
    let L6 = "&nbsp;".repeat(32);
    let L7 = "&nbsp;".repeat(32);
    let L8 = "&nbsp;".repeat(32);
    let L9 = pad("Thanks for your patronage", 32, "&nbsp;", 3);
    let LY = "&nbsp;".repeat(32);
    let LZ = "-".repeat(32);
    let LA = "&nbsp;".repeat(32);
    let LB = "&nbsp;".repeat(32);


    out += `<span>${LU}</span><br/>
                <span>${LUA}</span><br/>
                <span>${LV}</span><br/>
                <span>${L4}</span><br/>
                <span>${L5}</span><br/>
                <span>${LW}</span><br/>
                <span>${L6}</span><br/>
                <span>${L7}</span><br/>
                <span>${L8}</span><br/>
                <span>${L9}</span><br/>
                <span>${LY}</span><br/>
                <span>${LZ}</span><br/>
                <span>${LA}</span><br/>
                <span>${LB}</span><br/>`;

    return out;
  }
};

function pad(str, len, pad, dir) {
  const STR_PAD_LEFT = 1;
  const STR_PAD_RIGHT = 2;
  const STR_PAD_BOTH = 3;

  if (typeof(len) === "undefined") {
    len = 0;
  }
  if (typeof(pad) === "undefined") {
    pad = ' ';
  }
  if (typeof(dir) === "undefined") {
    dir = STR_PAD_RIGHT;
  }


  if (len + 1 >= str.length) {

    switch (dir) {

      case STR_PAD_LEFT:
        str = Array(len + 1 - str.length).join(pad) + str;
        break;

      case STR_PAD_BOTH:
        const padlen = len - str.length;
        let right = Math.ceil((padlen) / 2);
        let left = padlen - right;
        str = Array(left + 1).join(pad) + str + Array(right + 1).join(pad);
        break;

      default:
        str = str + Array(len + 1 - str.length).join(pad);
        break;

    } // switch

  }

  return str;

}
