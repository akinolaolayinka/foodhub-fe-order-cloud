import Toast from "./Toast";
import * as FormatterUtil from "./FormatterUtil";
import {InitializeNotifications, showNotification} from "./NotificationUtil";

const io = require('sails.io.js')( require('socket.io-client') );
let BASE_URL = "http://localhost:1337";
if (process.env.NODE_ENV ===  'production'){
  BASE_URL = "https://foodhubv2.herokuapp.com/";
}
io.sails.url = BASE_URL;
io.sails.transports = ['polling','websocket'];
io.sails.reconnection = true;
let _token, _vm;

io.socket.on('reconnect', (data)=>{
  console.log('reconnect ', data);
  connect(_token, _vm);
});

export const connect = (token, vm)=>{
  _token = token;
  _vm = vm;

  //we unsubscribe the server from all rooms then we re-subscribe him to all rooms

  io.socket.request({
    method: 'get',
    url: '/v1/chat/server/unsubscribe',
    data: {
      limit: 15
    },
    headers: {
      Authorization : `Bearer ${_token}`
    }
  }, function (resData, jwres) {
    if (jwres.error) {
      console.log("Error occurred while un-subscribing the admin from all clients rooms",jwres.statusCode); // => e.g. 403
      return;
    }

    console.log('Admin was un-subscribed from all rooms');
    io.socket.request({
      method: 'get',
      url: '/v1/chat/server/subscribe',
      data: {
        limit: 15
      },
      headers: {
        Authorization : `Bearer ${_token}`
      }
    }, function (resData, jwres) {
      if (jwres.error) {
        console.log(jwres.statusCode); // => e.g. 403
        return;
      }
      console.log('Admin was re-subscribed to all rooms');

      io.socket.on('new-message', function (payload) {
        //console.log(_vm.$route);
        const senderName = payload.data.sender.firstName;
        if (_vm.$route.name !== 'Chat Page'){
          Toast.SUCCESS(`<strong>${senderName} says ${payload.data.message}</strong>`, 4000, 'rounded');
        }

        _vm.$store.commit('INSERT_CHAT', payload.data);
        showNotification(`Message -> (${senderName})`, payload.data.message, {
          actions: [
            {action: 'reply', title: 'Reply'}
          ],
          data: payload.data
        });
      });

      io.socket.on('is-typing', function (payload) {
        _vm.$store.commit('UPDATE_ISTYPING', payload.data);
      });

      io.socket.on('new-customer', function (payload) {
        _vm.$store.commit('SET_CUSTOMER_ONLINE_STATUS', payload.data.user);
        const customerName = payload.data.user.firstName;
        Toast.SUCCESS(`${customerName} comes to market`, 4000, 'rounded');
      });

      io.socket.on('customer-leaves', function (payload) {
        _vm.$store.commit('SET_CUSTOMER_ONLINE_STATUS', payload.data.user);
        const customerName = payload.data.user.firstName;
        Toast.SUCCESS(`${customerName} leaves the market`, 2000, 'rounded');
      });

      io.socket.on('new-order', function (payload) {
        const amount = payload.data.shipment.payment.amount;
        const message = `We have a new order worth ${FormatterUtil.formatAsCurrency(amount)}`;
        Toast.SUCCESS(message, 30000, 'rounded');
        _vm.$store.commit('createShipment', payload.data.shipment);
        showNotification("New Order", `Amount = ${FormatterUtil.formatAsCurrency(amount)}`, {
          actions: [
            {action: 'orders', title: 'See Order'}
          ]
        });
      });

      io.socket.on('shipment-status-changed', function (payload) {
        const shipment = payload.data.shipment;
        if (shipment && shipment.status === 'user-cancelled'){
          const orderNo = shipment.order.orderNo;
          Toast.ERROR(`Order ${orderNo} was cancelled by the user`, 20000, 'rounded');
          showNotification("Order Cancelled", `Order ${orderNo} was cancelled by the user`);
          _vm.$store.commit('updateshipment', shipment);
        }
      });

    });

  });


  InitializeNotifications();
};


export const sendMessage = (payload)=>{
  return new Promise((resolve, reject)=>{
    io.socket.request({
      method: 'post',
      //url: '/v1/chat/client/messages',
      url: '/v1/chat/server/messages',
      data: payload,
      headers: {
        Authorization : `Bearer ${_token}`
      }
    }, function (resData, jwres) {
      if (jwres.error) {
        console.log(jwres.statusCode); // => e.g. 403
        return reject(jwres.error);
      }

      return resolve(resData.data)

    });
  });
};



export const signifyIsTyping = (payload)=>{
  return new Promise((resolve, reject)=>{
    io.socket.request({
      method: 'post',
      url: '/v1/chat/client/isTyping',
      data: payload,
      headers: {
        Authorization : `Bearer ${_token}`
      }
    }, function (resData, jwres) {
      if (jwres.error) {
        console.log(jwres.statusCode); // => e.g. 403
        return reject(jwres.error);
      }

      return resolve(resData.data)

    });
  });
};
