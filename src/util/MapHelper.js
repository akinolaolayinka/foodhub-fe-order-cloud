
module.exports = {

  ConvertArrayToMap : (array, key)=>{
    return array.reduce(function(map, obj) {
      map[obj[key]] = obj;
      return map;
    }, {});
  }
}
