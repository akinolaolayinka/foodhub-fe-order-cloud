import {HTTP} from './http-commons';
import {HTTP_BEARER} from './http-commons';


export default  {

  searchProductVariants : function (data, callback) {
    HTTP.post('/v1/search/', data)
      .then(function (response) {
        callback(null, response.data);
      })
      .catch(function (err) {
        console.log(err);
      })
  },


  getMeasurements : function (data, callback) {
    HTTP_BEARER().get('/v2/admin/measurements')
      .then(function (response) {
        callback(null, response.data);
      })
      .catch(function (err) {
        console.log(err);
        callback(err);
      })
  },




}
