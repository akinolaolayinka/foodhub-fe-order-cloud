/**
 * Created by User on 3/7/2018.
 */
import axios from 'axios';
let BASE_URL = "http://localhost:1337/";
import store from './../store/store';

if (process.env.NODE_ENV ===  'production'){
  BASE_URL = "https://foodhubv2.herokuapp.com/";
}
console.log(BASE_URL);

export const HTTP = axios.create({
  baseURL : BASE_URL,
  headers : {
    Authorization : 'Basic QWJTbG1EVXNhTGVZNUNIam51V0g2cTloR3RxWExCb3hfclRYeG1lLUdZVFlvYjQzUWE3ZEMzemZ3RzBEM3NrZ290WTFIdHRFWXNrM0ZlcEs6RU1NQWs5NkVTUkxGYXlLS0s1Vzlrd3F3OUJEcTBrdDMtUks2Tl82cG51RHVtc0hQZFZrNnhqNTVnUEJpb2cyQzNNd2xaSjFvOWlWb1I2S2E='
  }
});

export const HTTP_BEARER = () => {
  return axios.create({
    baseURL : BASE_URL,
    headers : {
      Authorization : `Bearer ${store.state.token}`
    }
  });
}
