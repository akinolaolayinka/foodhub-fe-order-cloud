import Toast from './Toast';
import OrdersHandler from './OrdersHandler';

module.exports = {

  repost : (vm, shipment, index)=>{
    console.log(shipment);
    OrdersHandler.createShipment(shipment, function (error, response) {
      if (error) {
        console.log(error);
        let err = {message: "Error occurred"};
        try {
          err = JSON.parse(error.responseText);
        } catch (e) {
        }
        Toast.Show(err.message, 4000);
        return;
      }
      Toast.SUCCESS('Order was placed successfully', 4000, 'rounded');
      vm.$store.dispatch('popShipmentFromCache', index);
      vm.$store.dispatch('createShipment', response);
    });
  }
}
