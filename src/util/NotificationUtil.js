import Toast from "./Toast";

let canNotify = false;
let _registration;
export const InitializeNotifications = () => {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    canNotify = false;
    Toast.INFO('This browser does not support desktop notification', 4000);
  }

  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    canNotify = true;
  }

  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== "denied") {
    Notification.requestPermission()
      .then(function (permission) {
        // If the user accepts, let's create a notification
        canNotify = permission === "granted";
      });
  }else{
    //Always request notification permission in Order cloud because it is critical
    Notification.requestPermission()
      .then(function (permission) {
        // If the user accepts, let's create a notification
        canNotify = permission === "granted";
      });
  }

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./static/js/serviceworker.js')
      .then(function (registration) {
        _registration = registration;
        //Toast.INFO('Service worker registration succeeded', 4000);
      })
      .catch((error) => {
        //Toast.ERROR('Service worker registration failed', 4000);
        console.log('ServiceWorker registration failed: ', error);
      })
  }

};

export const showNotification = (title, message, options = {}) => {
  const payload = {
    body: message,
    icon: options.icon || '../static/img/logo.png',
    //image: options.icon || '../static/img/logo.png',
    timestamp: Math.floor(Date.now()),
    silent: false,
    requireInteraction: true,
    vibrate: [400, 50, 400, 0, 400, 50, 400, 0, 400],
    data : options.data || {},
    actions : options.actions || [{action: 'open', title: 'Open'}],
  };
  if (canNotify) {
    if (_registration){
      _registration.showNotification(title, payload);
    }else{
      new Notification(title, payload);
    }
  }
};
