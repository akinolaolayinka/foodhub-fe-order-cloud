import ShipmentPerformanceUtil from './../ShipmentPerformanceUtil';
import {expect} from 'chai';
const dummyArray = [
  {
    "id": 1,
    "shipment": 95,
    "user": 1,
    "status": "created",
    "createdAt": "2018-07-27T23:00:00.000Z",
    "updatedAt": "2018-07-27T23:00:00.000Z"
  },
  {
    "id": 1,
    "shipment": 95,
    "user": 1,
    "status": "processing",
    "createdAt": "2018-07-27T23:05:00.000Z",
    "updatedAt": "2018-07-27T23:00:00.000Z"
  }
]
describe("#ShipmentPerformanceUtilTest", function () {
  describe("#evaluatePerformance", function () {
    it("#should return the performance of a shipment progression", function () {
      let score = ShipmentPerformanceUtil.evaluatePerformance(dummyArray);
      expect(score).to.be.an('object');
      //expect(score).to.equal(10);
    })
  })
})
