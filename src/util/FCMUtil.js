import firebase from "firebase/app";
import 'firebase/messaging';
import Toast from "./Toast";

let messaging;

export const initiateFirebase = (cb) => {
  configureFirebase();
  requestFCMPermission();
  getFCMToken(cb);
  monitorFCMTokenRefresh(cb);
};

export const configureFirebase = () => {
  const config = {
    apiKey : "AIzaSyD97hZuQcVBlk03yhqlwnykcYBj55TV6Fk",
    authDomain : "bodija-market.firebaseapp.com",
    databaseURL : "https://bodija-market.firebaseio.com",
    projectId : "bodija-market",
    storageBucket : "bodija-market.appspot.com",
    messagingSenderId : "12359841140"
  };
  firebase.initializeApp(config);

  // Retrieve Firebase Messaging object.
  messaging = firebase.messaging();

  // Add the public key generated from the console here.
  messaging.usePublicVapidKey("BARIT5FjuKAHtTKt_obnG7D-zjlotSApsN2VS-Pu3EeNJBSG6v9CuRD2R0JgG5BWL0-AmfXHL5XaRdX3Dw5de3s");

  // Handle incoming messages. Called when:
  // - a message is received while the app has focus
  // - the user clicks on an app notification created by a service worker
  // `messaging.setBackgroundMessageHandler` handler.
  messaging.onMessage(function (payload) {
    console.log('Message received. ', payload);
    const data = payload.data;
    Toast.INFO(data.message);
    //vm.$toastr.info(data.message, data.title, {});
  });

  return messaging;
};

export const requestFCMPermission = () => {
  messaging.requestPermission()
    .then(function () {
      //console.log('Notification permission granted.');
      // TODO(developer): Retrieve an Instance ID token for use with FCM.
      // ...
    })
    .catch(function (err) {
      console.log('Unable to get permission to notify.', err);
    });
};

export const getFCMToken = (cb) => {
  // Get Instance ID token. Initially this makes a network call, once retrieved
  // subsequent calls to getToken will return from cache.
  messaging.getToken()
    .then(function (currentToken) {
      if (currentToken) {
        cb(null, currentToken);
      } else {
        requestFCMPermission();
        console.log('No Instance ID token available. Request permission to generate one.');
      }
    })
    .catch(function (err) {
      console.log('An error occurred while retrieving token. ', err);
      cb(err);
    });
};


export const monitorFCMTokenRefresh = (cb) => {
  // Callback fired if Instance ID token is updated.
  messaging.onTokenRefresh(function () {
    messaging.getToken()
      .then(function (refreshedToken) {
        console.log('Token refreshed.');
        cb(null, refreshedToken);
      })
      .catch(function (err) {
        console.log('Unable to retrieve refreshed token ', err);
        cb(err);
      });
  });
};
