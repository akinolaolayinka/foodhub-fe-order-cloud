import Vue from 'vue'
import Router from 'vue-router'
const Login  = ()=> import(/*webpackChunkName: "Login"*/'@/components/authentication/Login');
const MainContainer  = ()=> import(/*webpackChunkName: "MainContainer"*/'@/components/main/MainContainer');
const Dashboard  = ()=> import(/*webpackChunkName: "Dashboard"*/'@/components/main/dashboard/Dashboard');
const NewPurchase  = ()=> import(/*webpackChunkName: "NewPurchase"*/'@/components/main/newpurchase/NewPurchase');
const Orders  = ()=> import(/*webpackChunkName: "Orders"*/'@/components/main/orders/Orders');
const UnSentPurchases  = ()=> import(/*webpackChunkName: "UnSentPurchases"*/'@/components/main/unsent/UnSentPurchases');
const ChatListComponent  = ()=> import(/*webpackChunkName: "ChatListComponent"*/'@/components/main/chat/ChatListComponent');
const ChatPageComponent  = ()=> import(/*webpackChunkName: "ChatPageComponent"*/'@/components/main/chat/ChatPageComponent');
const NotFound404  = ()=> import(/*webpackChunkName: "NotFound404"*/'@/components/main/NotFound404');


Router.mode = 'history';
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: MainContainer,
      children: [
        {
          path: '',
          name: "MainContainer",
          component: Dashboard
        },
        {
          path: 'dashboard',
          name: "Dashboard",
          component: Dashboard
        },
        {
          path: 'orders',
          name: 'Orders',
          component: Orders
        },
        {
          path: 'new-purchase',
          name: 'NewPurchase',
          component: NewPurchase
        },
        {
          path: 'unsent',
          name: 'UnSentPurchases',
          component: UnSentPurchases
        },
        {
          path: 'chats',
          name: 'Chats',
          component: ChatListComponent
        },
        {
          path: 'chats/client/:clientId',
          name: 'Chat Page',
          component: ChatPageComponent
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '*',
      name: 'NotFound404',
      component: NotFound404
    }
  ]
})
