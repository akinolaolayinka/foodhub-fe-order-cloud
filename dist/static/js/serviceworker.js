/**
 * Created by User on 12/4/2017.
 */

self.addEventListener('push', function (e) {
    var options = {
        body: 'This notification was generated from a push!',
        icon: 'images/example.png',
        vibrate: [100, 50, 100],
        data: {
            dateOfArrival: Date.now(),
            primaryKey: '2'
        },
        actions: [
            {
                action: 'explore', title: 'Explore this new world',
                icon: 'images/checkmark.png'
            },
            {
                action: 'close', title: 'Close',
                icon: 'images/xmark.png'
            },
        ]
    };
    e.waitUntil(
        self.registration.showNotification('Hello world!', options)
    );
});


self.addEventListener('notificationclick', function(event) {
  const data = event.notification.data;
  event.notification.close();

  switch (event.action){
    case 'reply':
      let sender = data.sender;
      clients.openWindow(`/chats/client/${sender.id}`);
      break;
    case 'open':
      clients.openWindow("/");
      break;
    case 'orders':
      clients.openWindow("/orders");
      break;
    default:
      clients.openWindow("/");
  }

}, false);
