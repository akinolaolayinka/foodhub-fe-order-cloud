// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
//import firebase from "firebase";

importScripts('https://www.gstatic.com/firebasejs/5.8.5/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.8.5/firebase-messaging.js');
// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
const config = {
  apiKey: "AIzaSyD97hZuQcVBlk03yhqlwnykcYBj55TV6Fk",
  authDomain: "bodija-market.firebaseapp.com",
  databaseURL: "https://bodija-market.firebaseio.com",
  projectId: "bodija-market",
  storageBucket: "bodija-market.appspot.com",
  messagingSenderId: "12359841140"
};
firebase.initializeApp(config);

// Retrieve Firebase Messaging object.
messaging = firebase.messaging();

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  const data = payload.data;
  // Customize notification here
  let notificationTitle = data.title;
  let notificationOptions = {
    body : data.message,
    icon : './static/img/logo.png',
    timestamp : Math.floor(Date.now()),
    silent : false,
    requireInteraction : true,
    vibrate : [400, 50, 400, 0, 400, 50, 400, 0, 400]
  };

  return self.registration.showNotification(notificationTitle, notificationOptions);
});



// [END background_handler]

